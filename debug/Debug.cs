﻿using System;
using Godot;

namespace TurnBasedRPG.debug;

internal static class Debug
{
    internal static void Assert(bool cond, string msg)
    {
        if (cond) return;
        GD.PrintErr(msg);
        throw new ApplicationException($"Assert Failed: {msg}");
    }
    
}