using Godot;
using TurnBasedRPG.entities.game;
using TurnBasedRPG.entities.scene_switcher;
using MainMenu = TurnBasedRPG.entities.gui.menu.main.MainMenu;
using SceneSwitcher = TurnBasedRPG.entities.scene_switcher.SceneSwitcher;
using SettingsFileAccess = TurnBasedRPG.util.SettingsFileAccess;
using SettingsManager = TurnBasedRPG.entities.settings.SettingsManager;

namespace TurnBasedRPG.entities;

public partial class Main : Node
{
	private PackedScene MainMenuScene { set; get; }
	
	private PackedScene GameScene { set; get; }
	
	private World GameWorld { set; get; }

	private MainMenu MainMenu { set; get; }

	
	public override void _Ready()
	{

		MainMenuScene = ResourceLoader.Load<PackedScene>("res://entities/gui/menu/main/MainMenu.tscn");
		GameScene = ResourceLoader.Load<PackedScene>("res://entities/game/World.tscn");
		
		GD.Randomize();
		SettingsFileAccess.LoadSettings();
		SettingsManager.Instance.ApplySettings();
		OpenMainMenu(SceneTransition.None);

	}

	private async void StartGame()
	{
		GameWorld = (World) await SceneSwitcher.Instance.SwitchScene(this, GameScene, 
			MainMenu, SceneTransition.Fade);
		GameWorld.Connect(World.SignalName.GameEnd, new Callable(this, MethodName.OpenMainMenu));
	}

	private async void OpenMainMenu(SceneTransition transition)
	{
		
		MainMenu = (MainMenu) await SceneSwitcher.Instance.SwitchScene(this, MainMenuScene, 
			GameWorld, transition);
		MainMenu.Connect(MainMenu.SignalName.GameStart, new Callable(this, MethodName.StartGame));
	}
}