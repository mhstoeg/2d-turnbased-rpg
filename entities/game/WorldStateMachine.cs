using System;
using TurnBasedRPG.entities.gui.menu.pause;
using TurnBasedRPG.entities.state;

namespace TurnBasedRPG.entities.game;

public partial class WorldStateMachine : BaseStateMachine
{
	private World World { set; get; }

	public override void _Ready()
	{
		World = GetNode<World>("..");
	}


	protected override void StateLogic(double delta)
	{
		switch (State)
		{
			case WorldState.Pause:
				break;
			case WorldState.Play:
				break;
		}
	}

	protected override Enum GetTransition(double delta)

	{
		var transition = base.GetTransition(delta);
		switch (State)
		{
			case WorldState.Pause:
				if (World.PauseMenu.StateMachine.IsState(PauseMenuState.Hidden))
					return WorldState.Play;
				break;
			case WorldState.Play:
				if (!World.PauseMenu.StateMachine.IsState(PauseMenuState.Hidden))
					return WorldState.Pause;
				break;
		}

		return transition;
	}

	protected override void EnterState(Enum newState, Enum oldState)
	{
		switch (newState)
		{
			case WorldState.Pause:
				World.Hud.Hide(); 
				break;
			case WorldState.Play:
				World.Hud.Show(); 
				break;
			default:
				throw new Exception("Entered invalid State, " + State);
		}
	}

	protected override void ExitState(Enum oldState, Enum newState)
	{
		switch (oldState)
		{
			case WorldState.Pause:
				break;
			case WorldState.Play:
				break;
			default:
				throw new Exception("Exited invalid State, " + State);
		}
	}
}