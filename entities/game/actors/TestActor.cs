using Godot;

namespace TurnBasedRPG.entities.game.actors;

public partial class TestActor : Actor 
{
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		Size = 1;
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
}