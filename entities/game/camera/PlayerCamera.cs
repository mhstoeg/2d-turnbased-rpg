using Godot;

namespace TurnBasedRPG.entities.game.camera;

public partial class PlayerCamera : Camera2D
{
	private Vector2 CameraSpeed { set; get;}
	private int CameraLerpSpeed { set; get;}
	private Vector2 ZoomMin { set; get;}
	private Vector2 ZoomMax { set; get;}
	private Vector2 ZoomSpeed { set; get;}
	private int ZoomLerpSpeed { set; get;}
	public Vector2 DefaultPosition { private set; get; }
	public Vector2 DefaultZoom { private set; get; }
	public Vector2 DesiredPosition { private set; get; }
	public Vector2 DesiredZoom { private set; get; }

	public override void _Ready()
	{
		// TODO make changeable in the Gameplay Settings Tab
		CameraSpeed = new Vector2(1500, 1500);
		CameraLerpSpeed = 10;
		ZoomMin = new Vector2(0.5f, 0.5f);
		ZoomMax = new Vector2(5f, 5f);
		ZoomSpeed = new Vector2(0.2f, 0.2f);
		ZoomLerpSpeed = 8;

		// Set default zoom and position
		DefaultPosition = GetViewportRect().Size / 2;
		DefaultZoom = Vector2.One;
		
		GlobalPosition = DefaultPosition;
		Zoom = DefaultZoom;
		
		DesiredPosition = GlobalPosition;
		DesiredZoom = Zoom;

	}

	public override void _Process(double delta)
	{
		base._Process(delta);
		CameraInput(delta);
		UpdateCamera(delta);
	}

	public override void _Input(InputEvent @event)
	{
		base._Input(@event);
		MouseInput(@event);
	}

	private void CameraInput(double delta)
	{
		// TODO Make the values scale based on the Window size
		if (!IsCurrent()) return;
		var inputVector = Vector2.Zero;
		inputVector.X = Input.GetActionStrength("camera_right") - Input.GetActionStrength("camera_left");
		inputVector.Y = Input.GetActionStrength("camera_down") - Input.GetActionStrength("camera_up");

		var deltaVector = new Vector2((float)delta, (float)delta);
		DesiredPosition += inputVector.Normalized() * CameraSpeed * DesiredZoom.Inverse() * deltaVector;
	}

	private void MouseInput(InputEvent @event)
	{
		// TODO Make the values scale based on the Window size, might already do this
		if (!IsCurrent()) return;
		
		if (@event is InputEventMouseButton)
		{
			// TODO Make sure that the zoom can never exceed the zoom min or max
			if (Input.IsActionPressed("camera_out") && DesiredZoom > ZoomMin)
				DesiredZoom -= ZoomSpeed * DesiredZoom;
			if (Input.IsActionPressed("camera_in") && DesiredZoom < ZoomMax)
				DesiredZoom += ZoomSpeed * DesiredZoom;
		}

		if (@event is not InputEventMouseMotion motion) return;
		if (Input.IsActionPressed("camera_drag"))
			DesiredPosition -= motion.Relative * DesiredZoom.Inverse();
	}

	private void UpdateCamera(double delta)
	{
		// TODO is there a cleaner way to do this?
		var zoomLerpX = Mathf.Lerp(Zoom.X, DesiredZoom.X, ZoomLerpSpeed * (float) delta);
		var zoomLerpY = Mathf.Lerp(Zoom.Y, DesiredZoom.Y, ZoomLerpSpeed * (float) delta);
		Zoom = new Vector2(zoomLerpX, zoomLerpY);

		var globalPositionLerpX = Mathf.Lerp(GlobalPosition.X, DesiredPosition.X, CameraLerpSpeed * (float) delta);
		var globalPositionLerpY = Mathf.Lerp(GlobalPosition.Y, DesiredPosition.Y, CameraLerpSpeed * (float) delta);
		GlobalPosition = new Vector2(globalPositionLerpX, globalPositionLerpY);
	}


	public void Reset()
	{
		// TODO should implement a Resettable interface for this
		GlobalPosition = DefaultPosition;
		Zoom = DefaultZoom;
		DesiredPosition = DefaultPosition;
		DesiredZoom = DefaultZoom;
	}
}