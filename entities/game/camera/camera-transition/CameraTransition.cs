using System.Threading.Tasks;
using Godot;

namespace TurnBasedRPG.entities.game.camera.camera_transition;

public partial class CameraTransition : Node
{
    private bool Transitioning { set; get; }

    private static void SwitchCamera(Camera2D camera)
    {
        camera.MakeCurrent();
    }


    private async Task Transition(Camera2D fromCamera, Camera2D toCamera, double duration)
    {
        // If a transition was called while already transitioning, return
        if (Transitioning) return;
        
        // Copy the parameters of the first camera to the transition camera
        var transCamera = (Camera2D) GetNode("TransitionCamera");
        transCamera.Zoom = fromCamera.Zoom;
        transCamera.Offset = fromCamera.Offset;
        transCamera.LightMask = fromCamera.LightMask;
        
        // Move transition camera to the first camera position
        transCamera.GlobalTransform = fromCamera.GlobalTransform;
        
        // Make transition camera current
        transCamera.MakeCurrent();

        Transitioning = true;
        
        // Move to the second camera, while also adjusting the parameters to match the second camera

        var tween = CreateTween().SetParallel();

        tween.SetEase(Tween.EaseType.InOut);
        tween.SetTrans(Tween.TransitionType.Cubic);

        tween.TweenProperty(transCamera, "global_transform", toCamera.GlobalTransform, duration).FromCurrent();
        tween.TweenProperty(transCamera, "zoom", toCamera.Zoom, duration).FromCurrent();
        tween.TweenProperty(transCamera, "offset", toCamera.Offset, duration).FromCurrent();
        
        // Wait for the tween to complete
        await ToSignal(tween, Tween.SignalName.Finished);
        
        // Make the second camera current
        toCamera.MakeCurrent();
        Transitioning = false;
    }

    // TODO possible to make this static??
    private async Task TransitionBetween(Camera2D cameraOne, Camera2D cameraTwo, bool keepZoom, double duration)
    {
        // TODO from and to cameras should be referenced to camera one and two otherwise this will not work
        var fromCamera = cameraOne.IsCurrent() ? cameraOne : cameraTwo;
        var toCamera = cameraOne.IsCurrent() ? cameraTwo : cameraOne;
        if (keepZoom)
                toCamera.Zoom = fromCamera.Zoom;
        await Transition(fromCamera, toCamera, duration);
    }
}