class_name CameraTransition extends Node

var transitioning: bool = false

func switch_camera(to: Camera2D) -> void:
	to.make_current()

func transition(from: Camera2D, to: Camera2D, duration: float = 1.0) -> void:
	# If a transition was called while already transitioning, return
	if transitioning: return
	# Copy the parameters of the first camera to transiton camera
	var trans_camera = $TransitionCamera
	trans_camera.zoom = from.zoom
	trans_camera.offset = from.offset
	trans_camera.light_mask = from.light_mask

	# Move our transition camera to the first camera position
	trans_camera.global_transform = from.global_transform

	# Make our transition camera current
	trans_camera.make_current()

	transitioning = true

	# Move to the second camera, while also adjusting the parameters to
	# match the second camera
	
	var tween : Tween = create_tween().set_parallel()
	
	tween.set_ease(Tween.EASE_IN_OUT)
	tween.set_trans(Tween.TRANS_CUBIC)
	
	tween.tween_property(trans_camera, "global_transform", to.global_transform, duration).from_current()
	tween.tween_property(trans_camera, "zoom", to.zoom, duration).from_current()
	tween.tween_property(trans_camera, "offset", to.offset, duration).from_current()

	# Wait for the tween to complete
	await tween.finished

	# Make the second camera current
	to.make_current()
	transitioning = false

func transition_between(camera_one : Camera2D, camera_two : Camera2D, keep_zoom : bool, duration : float = 1.0) -> void:
		if camera_one.is_current():
			if keep_zoom:
				camera_two.zoom = camera_one.zoom
			transition(camera_one, camera_two, duration)
		else:
			if keep_zoom:
				camera_one.zoom = camera_two.zoom
			transition(camera_two, camera_one, duration)
