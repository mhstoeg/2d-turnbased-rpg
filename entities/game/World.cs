using System;
using Godot;
using TurnBasedRPG.entities.scene_switcher;
using TurnBasedRPG.entities.settings.gameplay;
using TurnBasedRPG.entities.sound.sound_manager;
using TurnBasedRPG.entities.state;
using BaseLevel = TurnBasedRPG.entities.game.level.BaseLevel;
using GameplaySettings = TurnBasedRPG.entities.settings.gameplay.GameplaySettings;
using PauseMenu = TurnBasedRPG.entities.gui.menu.pause.PauseMenu;
using PlayerCamera = TurnBasedRPG.entities.game.camera.PlayerCamera;
using SceneSwitcher = TurnBasedRPG.entities.scene_switcher.SceneSwitcher;
using SoundManager = TurnBasedRPG.entities.sound.sound_manager.SoundManager;

namespace TurnBasedRPG.entities.game;

public partial class World : Node2D
{
	public BaseStateMachine StateMachine { get; private set; }
	private PackedScene StartLevel { get; set; }
	private double MouseSens { get; set; }
	private BaseLevel CurrentLevel { get; set; }
	private PlayerCamera PlayerCamera { get; set; }
	public PauseMenu PauseMenu { get; private set; }
	public CanvasLayer Hud { get; private set; }

	[Signal] public delegate void GameEndEventHandler(SceneTransition sceneTransition);
	
	public override void _Ready()

	{
		// Load music track and level
		SoundManager.Instance.SetMusic(ResourceLoader.Load<AudioStream>("res://assets/audio/music/ragnarok-8039.mp3"));
		StartLevel = ResourceLoader.Load<PackedScene>("res://entities/game/level/test_level_one/TestLevel.tscn");
		
		// Mouse sens
		var mouseSensSettingName = GameplaySettingType.MouseSens.ToString();
		MouseSens = GameplaySettings.Instance.GetSetting<double>(mouseSensSettingName);
		
		// Get Nodes
		StateMachine = GetNode<BaseStateMachine>("StateMachine");
		PlayerCamera = GetNode<PlayerCamera>("PauseableObjects/PlayerCamera");
		PauseMenu = GetNode<PauseMenu>("PauseLayer/PauseMenu");
		Hud = GetNode<CanvasLayer>("PauseableObjects/Hud");
		
		// Add start level to world
		CurrentLevel = (BaseLevel) StartLevel.Instantiate();
		AddChild(CurrentLevel);
		
		PlayerCamera.MakeCurrent();
		StateMachine.State = WorldState.Play;
		
		// Connect signals
		CurrentLevel.Connect(BaseLevel.SignalName.GoToLevel, new Callable(this, MethodName.OnGoToLevel));
		PauseMenu.Connect(PauseMenu.SignalName.GoToMain, new Callable(this, MethodName.OnGoToMain));
		GameplaySettings.Instance.Connect(GameplaySettings.SignalName.MouseSensChanged,
			new Callable(this, MethodName.OnMouseSensChanged));
	}



	private async void OnGoToLevel(PackedScene scene)
	{
		// TODO Needs to be tested
		SoundManager.Instance.PlaySound(SoundManagerSound.BtnEnter);
		CurrentLevel = (BaseLevel) await SceneSwitcher.Instance.SwitchScene(this, scene, 
			CurrentLevel, SceneTransition.Fade);
	}

	private void OnGoToMain()
	{
		SoundManager.Instance.PlaySound(SoundManagerSound.GameEnd);
		EmitSignal(SignalName.GameEnd, (int) SceneTransition.Dissolve);
	}


	private void Reset()
	{
		// TODO should implement a Resettable interface for this
		PlayerCamera.Reset();
	}

	private void OnMouseSensChanged(double value)
	{
		MouseSens = value;
	}


	
	
	


}
