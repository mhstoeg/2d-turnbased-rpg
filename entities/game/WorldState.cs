﻿namespace TurnBasedRPG.entities.game;

public enum WorldState
{
    Pause,
    Play
}