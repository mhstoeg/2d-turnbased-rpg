using System;
using System.Linq;
using Godot;
using Godot.Collections;
using TurnBasedRPG.entities.game.map.tilemap;
using TurnBasedRPG.entities.game.map.tiles;
using TurnBasedRPG.util;
using Vector2 = Godot.Vector2;

namespace TurnBasedRPG.entities.game.map;



public partial class GameMap : Node2D 
{
	private Dictionary<Vector2, Tile> Tiles{ set; get; } = new();
	private AStarTileMap TileMap{ get; set; }
	private Line2D Line { set; get; }
	private AStarTileMapDebug GameMapDebug { set; get; }
	
	public override void _Ready()
	{
		TileMap = GetNode<AStarTileMap>("TileMap");
		Line = GetNode<Line2D>("Line2D");
		GameMapDebug = GetNode<AStarTileMapDebug>("AStarTileMapDebug");

		const int layer = 0;


		// TODO Crate TileStack Mechani where each level can add in their own types of tiles specific to that
		PlaceTile(layer, TileManager.Instance.GetTile(TileLayout.Five), new Vector2I(0,0), TerrainType.Standard);
		PlaceTile(layer, TileManager.Instance.GetTile(TileLayout.Six), new Vector2I(1,0), TerrainType.SpecialOne);
		PlaceTile(layer, TileManager.Instance.GetTile(TileLayout.Six)?.RotateTile(TileRotation.South), new Vector2I(0,1), TerrainType.SpecialOne);
		PlaceTile(layer, TileManager.Instance.GetTile(TileLayout.Six).RotateTile(TileRotation.East), new Vector2I(1,1), TerrainType.SpecialTwo);

		TileMap.Update(layer);
	}

	// TODO Does not belong in this class
	public override void _Input(InputEvent @event)
	{
		base._Input(@event);
		if (Input.IsActionJustPressed("ui_accept"))
			GameMapDebug.Visible = !GameMapDebug.Visible;
		if (@event is not InputEventMouseButton) return;
		if (Input.IsActionJustPressed("mouse_left"))
		{
			var targetCell = GetGlobalMousePosition()/ TileMap.TileSet.TileSize;
			 targetCell = new Vector2((float) Math.Floor(targetCell.X), (float) Math.Floor(targetCell.Y));
			 // var player = GetNodeOrNull<Node2D>("TestTileMap/Test");
			 // var playerPosition = GameMap.MapToLocal(GameMap.LocalToMap(player.GlobalPosition));
			 // var exceptionUnits = new [] { player };

			 GD.Print(targetCell);
			 
				//  GameMap.MapToLocal((Vector2I)targetCell), player.Scale, exceptionUnits);
			 // Line.Position = new Vector2(0, 0); // Use offset to move line to center of tile
			 // Line.Points = pathPoints;
		}

		if (Input.IsActionJustPressed("mouse_right"))
		{
			GD.Print("Cleared Lines");
			Line.Points = null;
			
		}
	}

	public void PlaceTile(int layer, Tile tile, Vector2I coords, TerrainType terrainType)
	{
		if (tile == null)
		{
			GD.PushError("Tile cannot placed on layer " + layer + " at coords " + coords + ", tile is null!");
			return;
		}
		if (IsTileAtCoords(coords))
			RemoveTile(layer, tile, coords);

		coords *= tile.Size;

	    var mappedFloors = new Array<Vector2I>();
	    var mappedWalls = new Array<Vector2I>();
	    for (var index = 0; index < tile.Squares.Length; index++)
	    {
		    var newCoords = Tile.MapIndexToPolyomino(index, tile.Size.X, tile.Size.Y);
		    var tileMapCoord = new Vector2I(newCoords.X + coords.X, newCoords.Y + coords.Y);
		    var terrainSet = tile.Squares[index];
		    switch (terrainSet)
		    {
			    case TerrainSet.Wall:
				    mappedWalls.Add(tileMapCoord);
				    break;
			    case TerrainSet.Floor:
				    mappedFloors.Add(tileMapCoord);
				    break;
			    case TerrainSet.None:
				    break;
			    default:
				    GD.PushError("Missing switch case for TerrainSet " 
				                 + (int) terrainSet + " (" + terrainSet + "). Terrain has not been handled!");
				    break;
		    }
	    }

		TileMap.SetCellsTerrainConnect(layer, mappedWalls, (int) TerrainSet.Wall, (int) terrainType, false);
		TileMap.SetCellsTerrainConnect(layer, mappedFloors, (int) TerrainSet.Floor, (int) terrainType, false);

		Tiles.Add(coords, tile);
	}
	

	public void RemoveTile(int layer, Tile tile, Vector2I coords)
	{
		if (!IsTileAtCoords(coords)) return;
		coords *= tile.Size;
		for (var x = coords.X; x < coords.X + tile.Size.X; x++)
		{
			for (var y = coords.Y; y < coords.Y + tile.Size.Y; y++)
			{
				var cell = new Vector2I(x, y);
				TileMap.EraseCell(layer, cell);
			}
			
		}
		Tiles.Remove(coords);
	}

	
	public bool IsTileAtCoords(Vector2I coords)
	{
		return Tiles.ContainsKey(coords);
	}
	
	public Vector2I[] GetNeighbours(Vector2I coords)
	{
		var neighbourCoords = Calculate.CoordsWithinRadius(coords, new Vector2I(-1, 1),
			new Vector2I(-1, 1), 1);
		var neighbourCoordsList = neighbourCoords.ToList();
		foreach (var neighbourCoord in neighbourCoords)
		{
			if (!IsTileAtCoords(neighbourCoord))
				neighbourCoordsList.Remove(neighbourCoord);
		}
		return neighbourCoordsList.ToArray();
	}
	
}