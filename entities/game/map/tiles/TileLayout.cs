﻿namespace TurnBasedRPG.entities.game.map.tiles;
public enum TileLayout
{
	One,
	Two,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
}
