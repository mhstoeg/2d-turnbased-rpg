﻿namespace TurnBasedRPG.entities.game.map.tiles;

public enum TileRotation
{
	East,
	South,
	West
}