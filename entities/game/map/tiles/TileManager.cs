﻿using Godot;

namespace TurnBasedRPG.entities.game.map.tiles;

public partial class TileManager : Node
{
	public static TileManager Instance { get; private set; }
	private Tile[] Tiles { get; set; }
	public override void _Ready()
	{
		base._Ready();
        Instance = this;
        
        var tileSize = new Vector2I(4, 4);
        var w = TerrainSet.Wall; 
        var f = TerrainSet.Floor; 
        
		var tileOne = new Tile( new [] 
			{ 
				w, f, f, w, 
				f, f, f, w, 
				f, f, f, w, 
				f, f, f, w 
			}
			, tileSize);

		var tileTwo = new Tile(new [] 
			{ 
				w, f, f, w, 
				w, f, f, f, 
				w, f, f, f, 
				w, f, f, f 
			}
			, tileSize);
		
		var tileThree = new Tile(new [] 
			{ 
				w, f, f, w, 
				f, f, f, f, 
				f, f, f, f, 
				f, f, f, f 
			}
			, tileSize);
		
		var tileFour = new Tile(new [] 
			{ 
				w, w, w, w, 
				w, f, f, f, 
				w, f, f, f, 
				w, f, f, f 
			}
			, tileSize);
		
		var tileFive = new Tile(new [] 
			{ 
				w, w, w, w, 
				f, f, f, w, 
				f, f, f, w, 
				w, f, f, w 
			}
			, tileSize);
		
		var tileSix = new Tile(new [] 
			{ 
				w, w, w, w, 
				f, f, f, f, 
				f, f, f, f, 
				f, f, f, f 
			}
			, tileSize);
		
		var tileSeven = new Tile(new [] 
			{ 
				w, f, f, w, 
				w, f, f, w, 
				w, f, f, w, 
				w, f, f, w 
			}
			, tileSize);
		
		var tileEight = new Tile(new [] 
			{ 
				w, w, w, w, 
				f, f, f, f, 
				f, f, f, f, 
				w, f, f, w 
			}
			, tileSize);
		
		var tileNine = new Tile(new [] 
			{ 
				w, f, f, w, 
				f, f, f, f, 
				f, f, f, f, 
				w, f, f, w 
			}
			, tileSize);
		
		Tiles = new[] { tileOne, tileTwo, tileThree, tileFour, tileFive, tileSix, tileSeven, tileEight, tileNine};
	}

	public Tile GetTile(TileLayout tileLayout)
	{
		var tileIndex = (int)tileLayout;
		if (tileIndex < 0 || tileIndex >= Tiles.Length) return null;
		return Tiles[(int)tileLayout];
	}
}