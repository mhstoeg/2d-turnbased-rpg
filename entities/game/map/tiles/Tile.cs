﻿using System;
using Godot;

namespace TurnBasedRPG.entities.game.map.tiles;
public partial class Tile : Node
{
	
	public TerrainSet[] Squares { private set; get; }
	public Vector2I Size { private set; get; }

	public Tile(TerrainSet[] squares, Vector2I size)
	{
		Squares = squares;
		Size = size;
	}

	public Tile RotateTile(TileRotation rotation)
	{
		int rotations;
		switch (rotation)
		{
			case TileRotation.East:
				rotations = 1;
				break;
			case TileRotation.South:
				rotations = 2;
				break;
			case TileRotation.West:
				rotations = 3;
				break;
			default:
				throw new ArgumentOutOfRangeException(nameof(rotation), rotation, null);
		}
		return new Tile(RotatePolyomino(Squares, Size, rotations), Size);
	}
	

	private T[] RotatePolyomino<T>(T[] polyominoAsArray, Vector2I arraySize, int rotations, bool clockwise = true)
	{
		while (true)
		{
			if (rotations <= 0) return polyominoAsArray;
			var height = arraySize.Y;
			var width = arraySize.X;
			var result = new T[height * width];

			for (var row = 0; row < height; row++)
			{
				for (var col = 0; col < width; col++)
				{
					int index;

					index = row * width + col;

					int newRow;
					int newCol;
					int newIndex;

					if (clockwise)
					{
						newRow = col;
						newCol = height - (row + 1);
					}
					else
					{
						newRow = width - (col + 1);
						newCol = height;
					}

					newIndex = newRow * height + newCol;
					result[newIndex] = polyominoAsArray[index];
				}
			}

			polyominoAsArray = result;
			rotations -= 1;
		}
	}
	
	public static Vector2I MapIndexToPolyomino(int index, int twoDArrayHeight, int twoDArrayLength) {
		var xCoord = index;
		var yCoord = 0;
		if (index >= twoDArrayLength) 
			xCoord = index % twoDArrayLength;
		if (index >= twoDArrayHeight)
			yCoord = (index - index % twoDArrayHeight) / twoDArrayHeight;
		return new Vector2I(xCoord, yCoord);
	}
}
