﻿namespace TurnBasedRPG.entities.game.map.tiles;

public enum TerrainSet
{
	Wall,
	Floor,
	None,
}