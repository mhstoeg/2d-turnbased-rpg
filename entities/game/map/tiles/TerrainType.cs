﻿namespace TurnBasedRPG.entities.game.map.tiles;

public enum TerrainType
{
	Standard,
	SpecialOne,
	SpecialTwo
}