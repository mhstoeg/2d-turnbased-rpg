#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using Godot;
using TurnBasedRPG.util.paring_function;
using Vector2 = Godot.Vector2;

namespace TurnBasedRPG.entities.game.map.tilemap;

public enum Groups
{
    Unit,
    Obstacle
}
public partial class AStarTileMap : TileMap
{



    private Vector2I[] CardinalDirections { set; get; } = { Vector2I.Up, Vector2I.Down, Vector2I.Left, Vector2I.Right };
    private double PairingLimit { set; get; } = Math.Pow(2, 30);

    private static ParingMethod CurrentPairingMethod { set; get; } = ParingMethod.SzudzikImproved;

    private bool Diagonals { set; get; } = true;

    //TODO convert to AStartGrid2D??
    public AStar2D AStar { private set; get; } = new();


    // TODO should it be using Node2D or can we get it to use GodotObject
    private List<Node2D> Obstacles { set; get; } = new();
    private List<Node2D> Units { set; get; } = new();

    public override void _Ready()
    {
        base._Ready();
        
        // TODO add layer functionality
        const int layer = 0;

        
        Update(layer);
    }

    public void Update(int layer)
    {
        CreatePathfindingPoints(layer);
        var unitNodes = GetTree().GetNodesInGroup(Groups.Unit.ToString());
        foreach (var unitNode in unitNodes)
        {
            if (unitNode.GetType() == typeof(Node2D))
                AddUnit((Node2D)unitNode);
        }

        var obstacleNodes = GetTree().GetNodesInGroup(Groups.Obstacle.ToString());
        foreach (var obstacleNode in obstacleNodes)
        {
           if (obstacleNode.GetType() == typeof(Node2D))
            AddObstacle((Node2D)obstacleNode);
        }
    }

    private static int GetPoint(Vector2 pointPosition)
    {
        return ParingFunction.PairVector2(pointPosition, CurrentPairingMethod);
    }

    private void CreatePathfindingPoints(int layer)
    {
        AStar.Clear();
        var usedCellPositions = GetUsedCellGlobalPositions(layer);
        foreach (var cellPosition in usedCellPositions)
        {
            if (!CellHasCollision(layer, cellPosition))
                AStar.AddPoint(GetPoint(cellPosition), cellPosition);
        }

        foreach (var cellPosition in usedCellPositions)
        {
            if (!CellHasCollision(layer, cellPosition))
                ConnectCardinals(cellPosition);
        }
    }

    private bool CellHasCollision(int layer, Vector2 cellPosition)
    {
        var tileData = GetCellTileData(layer, LocalToMap(cellPosition));
        if (tileData == null) return false;
        return tileData.GetCollisionPolygonsCount(layer) > 0;
    }
    public void AddObstacle(Node2D obstacle)
    {
        Obstacles.Add(obstacle);
        if (obstacle.IsConnected(Node.SignalName.TreeExited, new Callable(this, MethodName.RemoveObstacle))) return;

        void RemoveObstacleAction()
        {
            RemoveObstacle(obstacle);
        }

        var error = obstacle.Connect(Node.SignalName.TreeExited, Callable.From(RemoveObstacleAction));
        if (error != 0) GD.PushError(obstacle + ": failed connect() function");
    }

    public void RemoveObstacle(Node2D obstacle)
    {
       Obstacles.Remove(obstacle); 
    }

    public void AddUnit(Node2D unit)
    {
        Units.Add(unit);
        if (unit.IsConnected(Node.SignalName.TreeExited, new Callable(this, MethodName.RemoveUnit))) return;

        void RemoveUnitAction()
        {
            RemoveUnit(unit);
        }
        var error = unit.Connect(Node.SignalName.TreeExited, Callable.From(RemoveUnitAction));
        if (error != 0) GD.PushError(unit + ": failed connect() function");

    }

    public void RemoveUnit(Node2D unit)
    {
        Units.Remove(unit);
    }

    public bool PositionHasObstacle(Vector2 obstaclePosition, Vector2? ignoredObstaclePosition = null)
    {
        if (obstaclePosition == ignoredObstaclePosition) return false;
        foreach (var obstacle in Obstacles)
        {
            var occupiedPoints = DetermineObstacleOccupiedPoints(obstacle.Scale, obstacle.GlobalPosition);
            foreach (var pointPosition in occupiedPoints)
            {
                if (pointPosition == obstaclePosition) return true;
            }
        }
        return false;
    }

    public bool PositionHasUnit(Vector2 unitPosition, Node2D[]? exceptionUnits = null)
    {
        if (exceptionUnits!= null)
            foreach (var exceptionUnit in exceptionUnits)
            {
                var occupiedPoints = DetermineObstacleOccupiedPoints(exceptionUnit.Scale, exceptionUnit.GlobalPosition);
                foreach (var pointPosition in occupiedPoints)
                {
                if (pointPosition == unitPosition) return false;
                }
            }

        foreach (var unit in Units)
        {
            var occupiedPoints = DetermineObstacleOccupiedPoints(unit.Scale, unit.GlobalPosition);
            foreach (var pointPosition in occupiedPoints)
            {
                if (pointPosition == unitPosition) return true;
            }
        }
        return false;
    }

    public Vector2[] GetAStarPathAvoidingObstaclesAndUnits(Vector2 startPosition, Vector2 endPosition, Vector2 obstacleScale,
        Node2D[]? exceptionUnits = null, int maxDistance = -1)
    {
        SetObstaclesPointsDisabled(true);
        SetUnitsPointsDisabled(true, exceptionUnits);
        SetUnoccupiablePointsDisabled(true, obstacleScale, exceptionUnits);
        // TODO use GetAStarPath??
        var aStarPath = AStar.GetPointPath(GetPoint(startPosition), GetPoint(endPosition));
        SetObstaclesPointsDisabled(false);
        SetUnitsPointsDisabled(false, exceptionUnits);
        SetUnoccupiablePointsDisabled(false, obstacleScale, exceptionUnits);
        return SetPathLength(aStarPath, maxDistance);
    }

    public Vector2[] GetAStarPathAvoidingObstacles(Vector2 startPosition, Vector2 endPosition, int maxDistance = -1)
    {
        SetObstaclesPointsDisabled(true);
        var pathPoints = AStar.GetPointPath(GetPoint(startPosition), GetPoint(endPosition));
        SetObstaclesPointsDisabled(false);
        return SetPathLength(pathPoints, maxDistance);
    }

    private Vector2[] StopPathAtUnit(Vector2[] potentialPathPoints)
    {
        // TODO better solution?
        var resizedPathPoints = new List<Vector2>();
        for (var i = 1; i < potentialPathPoints.Length; i++)
        {
            var point = potentialPathPoints[i];
            resizedPathPoints.Add(point);
            if (PositionHasUnit(point)) break;
        }

        return resizedPathPoints.ToArray();
    }

    public Vector2[] GetAStarPath(Vector2 startPosition, Vector2 endPosition, int maxDistance = -1)
    {
        var aStarPath = AStar.GetPointPath(GetPoint(startPosition), GetPoint(endPosition));
        return SetPathLength(aStarPath, maxDistance);
    }

    private Vector2[] SetPathLength(Vector2[] pointPath, int maxDistance = -1)
    {
        if (maxDistance < 0) return pointPath;
        var newSize = Math.Min(pointPath.Length, maxDistance);

        // TODO better solution
        var resizedPointPath = new Vector2[newSize];
        // Copy Array
        for (var i = 0; i < newSize; i++)
            resizedPointPath[i] = pointPath[i];

        return resizedPointPath;
    }

    void SetObstaclesPointsDisabled(bool value)
    {
        foreach (var obstacle in Obstacles)
        {
            foreach (var pointPosition in DetermineObstacleOccupiedPoints(obstacle.Scale, obstacle.GlobalPosition))
            {
                AStar.SetPointDisabled(GetPoint(pointPosition), value);
            }
        }
    }



    private void SetUnitsPointsDisabled(bool value, Node2D[] exceptionUnits)
    {
        foreach (var unit in Units)
        {
            if (exceptionUnits.Contains(unit) || exceptionUnits.Contains(unit.Owner)) continue;
            foreach (var pointPosition in DetermineObstacleOccupiedPoints(unit.Scale, unit.GlobalPosition))
            {
                AStar.SetPointDisabled(GetPoint(pointPosition), value);
            }
        }
        
    }

    private void SetUnoccupiablePointsDisabled(bool value, Vector2 obstacleScale, Node2D[] exceptionUnits)
    {
        var pointIds = AStar.GetPointIds();
        foreach (var pointId in pointIds)
        {
            var pointPosition = AStar.GetPointPosition(pointId);
            if (ObstacleCanOccupy(obstacleScale, pointPosition, exceptionUnits)) continue;
            AStar.SetPointDisabled(GetPoint(pointPosition), value);
        }

    }
    private Vector2[] DetermineObstacleOccupiedPoints(Vector2 obstacleScale, Vector2 obstaclePosition)
    {

        var localOccupiedPoints = DetermineObstacleOccupiedPointsAtPosition(obstacleScale, obstaclePosition);

        return localOccupiedPoints;
    }

    public bool ObstacleCanOccupy(Vector2 obstacleScale, Vector2 position, Node2D[] exceptionUnits)
    {
        var xSize = (int)Mathf.Ceil(obstacleScale.X);
        var ySize = (int)Mathf.Ceil(obstacleScale.Y);
        var dimensions = xSize * ySize;
        var localOccupiedPoints = DetermineObstacleOccupiedPointsAtPosition(obstacleScale, position);
        var newVec = localOccupiedPoints.ToList();
        foreach (var point in localOccupiedPoints)
        {
            if (PositionHasUnit(point, exceptionUnits))
            {
                newVec.Remove(point);
            }
        }
        return newVec.Count >= dimensions;

    }

    
    public Vector2[] DetermineObstacleOccupiedPointsAtPosition(Vector2 obstacleScale, Vector2 position)
    {
        var xSize = (int)Mathf.Ceil(obstacleScale.X);
        var ySize = (int)Mathf.Ceil(obstacleScale.Y);

        var mapObstaclePosition = LocalToMap(position);

        var localOccupiedPoints = new List<Vector2>();
        for (var x = mapObstaclePosition.X; x < mapObstaclePosition.X + xSize; x++)
        {
            for (var y = mapObstaclePosition.Y; y < mapObstaclePosition.Y + ySize; y++)
            {
                var localPoint = MapToLocal(new Vector2I(x, y));
                if (!HasPoint(GetPoint(localPoint))) continue;
                localOccupiedPoints.Add(localPoint);
            }
        }

        return localOccupiedPoints.ToArray();
    }


    private Vector2[] GetFloodfillPositions(Vector2 startPosition, int minRange, int maxRange, bool skipObstacles = true,
        bool skipUnits = true, bool returnCenter = false)
    {
        var floodFillPositions = new List<Vector2>();
        var checkingPositions = new List<Vector2> { startPosition };

        while (checkingPositions.Any())
        {
            // TODO better way?
            // Pop the last element
            var lastCheckingPositionIdx = checkingPositions.Count - 1;
            var currentPosition = checkingPositions.Last();
            checkingPositions.RemoveAt(lastCheckingPositionIdx);
            
            if (skipObstacles && PositionHasObstacle(currentPosition, startPosition)) continue;
            // TODO this was changed when editing PositionHasUnit, take startPositon
            //if (skipUnits && PositionHasUnit(currentPosition, startPosition)) continue;
            if (skipUnits && PositionHasUnit(currentPosition)) continue;
            if (floodFillPositions.Contains(currentPosition)) continue;

            var currentPoint = GetPoint(currentPosition);
            if (!HasPoint(currentPoint)) continue;
            if (AStar.IsPointDisabled(currentPoint)) continue;

            var distance = currentPosition - startPosition;
            var gridDistance = GetGridDistance(distance);
            if (gridDistance > maxRange) continue;
            
            floodFillPositions.Add(currentPosition);
            
            // Diagonal directions?
            foreach(var direction in CardinalDirections)
            {
                var newPosition = MapToLocal(LocalToMap(currentPosition) + direction);
                if (skipObstacles && PositionHasObstacle(newPosition)) continue;
                if (skipUnits && PositionHasUnit(newPosition)) continue;
                if (floodFillPositions.Contains(newPosition)) continue;

                var newPoint = GetPoint(newPosition);
                if (!HasPoint(newPoint)) continue;
                if (AStar.IsPointDisabled(newPoint)) continue;
                
                checkingPositions.Add(newPosition);
                
            }
        }
        


        if (!returnCenter)
            floodFillPositions.Remove(startPosition);

        var floodFillPositionsSize = floodFillPositions.Count;
        for (var i = 0; i < floodFillPositionsSize; i++)
        {
            // Loop through the positions backwards
            var floodFillPosition = floodFillPositions[floodFillPositionsSize - i - 1];
            var distance = floodFillPosition - startPosition;
            var gridDistance = GetGridDistance(distance);
            if (gridDistance < minRange)
                floodFillPositions.Remove(floodFillPosition); // Since we are modifying the array here
        }

        return floodFillPositions.ToArray();
    }

    private Vector2[] PathDirections(Vector2[] path)
    {
        // Convert a path into a directional vectors whose sum would be path[length-1]
        var directions = new List<Vector2>();
        for (var i = 0; i < path.Length; i++)
            directions.Add(path[i] - path[i - 1]);
        return directions.ToArray();
    }



    private bool HasPoint(int pointId)
    {
        return AStar.HasPoint(pointId);
    }

    private Vector2[] GetUsedCellGlobalPositions(int layer)
    {
        var cells = GetUsedCells(layer);
        return cells.Select(cell => GlobalPosition + MapToLocal(cell)).ToArray();
    }

    private IEnumerable<Vector2I> GetDiagonalDirections()
    {
        if (!Diagonals) return new Vector2I[] { };
        var diagonalDirection = (Vector2I) Vector2.One;
        var mirroredDiagonalVector = diagonalDirection;
        mirroredDiagonalVector.X *= -1;
        // Mirror the vector along a single axis, either -X or -Y will work, but cannot be both,
        // otherwise both diagonals are inverse of each other
        var diagonalDirections = new []{diagonalDirection, mirroredDiagonalVector};
        return diagonalDirections;
    }

    private bool HasCollisionInDiagonalDirection(Vector2 pointPositionInLocal, Vector2I direction)
    {
        if (direction.X == 0) return false;
        if (direction.Y == 0) return false;
        var pointPositionInMap = LocalToMap(pointPositionInLocal);
        var xNeighbourCellInMap = GetNeighborCell(pointPositionInMap,
            direction.X > 0 ? TileSet.CellNeighbor.RightSide: TileSet.CellNeighbor.LeftSide);
        var yNeighbourCellInMap = GetNeighborCell(pointPositionInMap,
            direction.Y > 0 ? TileSet.CellNeighbor.BottomSide: TileSet.CellNeighbor.TopSide);
        
        var xNeighbourHasCollision = CellHasCollision(0, MapToLocal(xNeighbourCellInMap));
        var yNeighbourHasCollision = CellHasCollision(0, MapToLocal(yNeighbourCellInMap));

        return xNeighbourHasCollision && yNeighbourHasCollision;
    }

    private void ConnectCardinals(Vector2 pointPositionInLocal)
    {
        var directions = CardinalDirections.Concat(GetDiagonalDirections()).ToList();
        foreach (var direction in directions)
        {
            var cellInMap = LocalToMap(pointPositionInLocal) + direction;
            var cellInLocal = MapToLocal(cellInMap);

            if (!CardinalDirections.Contains(direction)) // Is a Diagonal Direction
                if (HasCollisionInDiagonalDirection(pointPositionInLocal, direction)) continue;
            
            var centerPoint = GetPoint(pointPositionInLocal);
            var cardinalPoint = GetPoint(cellInLocal);
            if (cardinalPoint != centerPoint && HasPoint(cardinalPoint))
                AStar.ConnectPoints(centerPoint, cardinalPoint);
        }

    }

    private float GetGridDistance(Vector2 distance)
    {
        var vec = (Vector2)LocalToMap(distance).Abs();
        var x = Math.Floor(vec.X);
        var y = Math.Floor(vec.Y);
        return (float) (x + y);
    }
}