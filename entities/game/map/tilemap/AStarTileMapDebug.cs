using System.Collections.Generic;
using Godot;

namespace TurnBasedRPG.entities.game.map.tilemap;

public partial class AStarTileMapDebug : Control
{
	private AStarTileMap AStarTileMap { set; get; }
	private const int CircleRadius = 2;
	private const int LineWidth = 1;
	public override void _Ready()
	{
		AStarTileMap = GetNode<AStarTileMap>("../TileMap");
	}

	private bool PostionHasObstacle(Vector2 obstaclePosition)
	{
		return AStarTileMap.PositionHasObstacle(obstaclePosition) || AStarTileMap.PositionHasUnit(obstaclePosition);
	}

	public override void _Draw()
	{
		base._Draw();
		//var offset = AStarTileMap.TileSet.TileSize/2;
		var offset = new Vector2I(0, 0);
		foreach (var point in AStarTileMap.AStar.GetPointIds())
        {
	        if (AStarTileMap.AStar.IsPointDisabled(point)) continue;
	        var pointPosition = AStarTileMap.AStar.GetPointPosition(point);
	        if (PostionHasObstacle(pointPosition)) continue;

	        DrawCircle(pointPosition + offset, CircleRadius, Colors.White);

	        var pointConnections = AStarTileMap.AStar.GetPointConnections(point);
	        var connectedPositions = new List<Vector2>();
	        foreach (var connectedPoint in pointConnections)
	        {
		        if (AStarTileMap.AStar.IsPointDisabled(connectedPoint)) continue;
		        var connectedPointPosition = AStarTileMap.AStar.GetPointPosition(connectedPoint);
		        if (PostionHasObstacle(connectedPointPosition)) continue;
		        connectedPositions.Add(connectedPointPosition);
	        }

	        foreach (var connectedPosition in connectedPositions)
	        {
		        DrawLine(pointPosition + offset, connectedPosition + offset, Colors.White, LineWidth);
	        }
        }
	}

}