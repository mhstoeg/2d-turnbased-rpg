using Godot;

namespace TurnBasedRPG.entities.game.level.test_level_two;

public partial class TestLevelTwo : entities.game.level.BaseLevel
{
	public override void _Ready()
	{
		LevelName = "LevelTwo";
	}

	public override void _Input(InputEvent @event)
	{
		base._Input(@event);
		if (@event.IsActionPressed("ui_accept"))
		{
		}
	}
}
