using Godot;

namespace TurnBasedRPG.entities.game.level;

public partial class BaseLevel : Node
{
	protected string LevelName { get; set; }

	protected bool Loaded { get; set; }
	
	protected TileMap GameTileMap { get; set; }
	
	[Signal] public delegate void LevelLoadedEventHandler();
	[Signal] public delegate void GoToLevelEventHandler();
	


	public override void _Ready()
	{
		base._Ready();
		LevelName = "BaseLevel";
		Loaded = false;

		GameTileMap = GetNode<TileMap>("GameTileMap");
	}


	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		if (Loaded) return;
		Loaded = true;
		EmitSignal(SignalName.LevelLoaded);
	}

	public override void _Input(InputEvent @event)
	{
		base._Input(@event);
	}

}