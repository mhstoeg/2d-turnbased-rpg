using Godot;

namespace TurnBasedRPG.entities.game.level.test_level_one;

public partial class TestLevel : entities.game.level.BaseLevel
{
	public override void _Ready()
	{
		LevelName = "LevelOne";
	}

	public override void _Input(InputEvent @event)
	{
		base._Input(@event);
		if (@event.IsActionPressed("ui_accept"))
		{
		}
	}
}
