using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Godot;
using TurnBasedRPG.entities.sound.sound_queue;

namespace TurnBasedRPG.entities.sound.sound_pool;

[Tool]
public partial class SoundPool : Node
{
	private List<SoundQueue> Sounds { set; get; } = new ();
	private RandomNumberGenerator Random { set; get; } = new ();
	private int LastIndex { set; get; } = -1;
	
	public override void _Ready()
	{
		foreach (var child in GetChildren())
		{
			if (child is SoundQueue soundQueue)
				Sounds.Add(soundQueue);
		}
	}

	public override string[] _GetConfigurationWarnings()
	{
		var numberOfSoundQueueChildren = 0;
		foreach (var child in GetChildren())
		{
			if (child is SoundQueue)
				numberOfSoundQueueChildren++;
		}
		return numberOfSoundQueueChildren < 2 
			? new [] { "Expected two or more children of type SoundQueue." } : Array.Empty<string>();
	}

	public async void PlayRandomSound()
	{
		await PlayRandomSound(false);
	}

	public async Task PlayRandomSound(bool toAwait)
	{
		int index;
		do
		{
			index = Random.RandiRange(0, Sounds.Count - 1);
		} 
		while (index == LastIndex);

		LastIndex = index;
		Sounds[index].PlaySound();
		if (toAwait) await ToSignal(Sounds[index], AudioStreamPlayer.SignalName.Finished);
	}


}