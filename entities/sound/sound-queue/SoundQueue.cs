using System.Collections.Generic;
using System.Threading.Tasks;
using Godot;

namespace TurnBasedRPG.entities.sound.sound_queue;

[Tool]
public partial class SoundQueue : Node
{

	private int Next { set; get; }
	private List<AudioStreamPlayer> AudioStreamPlayers { set; get; } = new ();

	[Export] private int Count { get; set; } = 1;

	public override void _Ready()
	{
		
		if (GetChildCount() == 0)
			GD.PrintErr("No AudioStreamPlayer child found");
		
		var child = GetChild(0);
		if (child is not AudioStreamPlayer audioStreamPlayer) return;
		AudioStreamPlayers.Add(audioStreamPlayer);
			
		for (var i = 0; i < Count; i++)
		{
			var duplicate = audioStreamPlayer.Duplicate() as AudioStreamPlayer;
			AddChild(duplicate);
			AudioStreamPlayers.Add(duplicate);
		}
	}

	public override string[] _GetConfigurationWarnings()
	{
		if (GetChildCount() == 0)
			return new [] { "No children found. Expected on AudioStreamPlayer child." };
		return GetChild(0) is not AudioStreamPlayer ? 
			new [] { "Expected first child to be an AudioStreamPlayer" } : base._GetConfigurationWarnings();
	}

	public async void PlaySound()
	{
		await PlaySound(false);
	}

	public async Task PlaySound(bool toAwait)
	{
		if (AudioStreamPlayers[Next].Playing) return;
		AudioStreamPlayers[Next++].Play();
		Next %= AudioStreamPlayers.Count - 1;
		if (toAwait) await ToSignal(AudioStreamPlayers[Next], AudioStreamPlayer.SignalName.Finished);
	}
	
}