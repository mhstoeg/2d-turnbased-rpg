﻿namespace TurnBasedRPG.entities.sound.sound_manager;

public enum SoundManagerBus
{
	Master,
	Music,
	Sfx
}