﻿namespace TurnBasedRPG.entities.sound.sound_manager;

public enum SoundManagerSound
{
	BtnEnter,
	BtnClick,
	GameEnd
}