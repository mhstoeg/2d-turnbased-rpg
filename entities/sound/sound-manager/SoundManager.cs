using System.Threading.Tasks;
using Godot;
using SoundQueue = TurnBasedRPG.entities.sound.sound_queue.SoundQueue;

namespace TurnBasedRPG.entities.sound.sound_manager;


public partial class SoundManager : Node
{
	public static SoundManager Instance { get; private set; }
	

	public override void _Ready()
	{
		Instance = this;
	}

	public async void PlaySoundQueue(SoundManagerQueueSound sound)
	{
		await PlaySoundQueue(sound, false);
	}

	public async Task PlaySoundQueue(SoundManagerQueueSound sound, bool toAwait)
	{
		var soundName = sound + "SoundQueue";
		var soundNode = GetNodeOrNull<SoundQueue>(soundName);
		if (toAwait) await soundNode?.PlaySound(true)!; else soundNode?.PlaySound();
	}

	public async void PlaySound(SoundManagerSound sound)
	{
		await PlaySound(sound, false);
	}
	
	public async Task PlaySound(SoundManagerSound sound, bool toAwait)
	{
		var soundNode = GetNodeOrNull<AudioStreamPlayer>(sound.ToString());
		soundNode?.Play();
        if (toAwait) await ToSignal(soundNode, AudioStreamPlayer.SignalName.Finished);
	}

	public void SetMusic(AudioStream stream)
	{
		var musicPlayer = GetNodeOrNull<AudioStreamPlayer>("Music");
		SetPlayer(musicPlayer, stream);
	}


	public void FadeMusic(Tween tween, double duration, float finalVolumeDb)
	{
		var musicPlayer = GetNodeOrNull<AudioStreamPlayer>("Music");
		FadeSound(musicPlayer, tween, duration, finalVolumeDb);
	}

    public void SetBusVol(int busIdx, float vol)
	{
		AudioServer.SetBusVolumeDb(busIdx, vol);
	}
    
	private void SetPlayer(AudioStreamPlayer player, AudioStream stream)
	{
		player.Stream = stream;
		player.Play();
		
	}
    private void FadeSound(AudioStreamPlayer player, Tween tween, double duration, float finalVolumeDb)
    {
        tween.TweenProperty(player, "volume_db", finalVolumeDb, duration);
    }

}