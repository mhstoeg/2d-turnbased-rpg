using System.Threading.Tasks;
using Godot;

namespace TurnBasedRPG.entities.scene_switcher;

public partial class TransitionAnimation : Node
{

	public static TransitionAnimation Instance { get; private set; }

    public override void _Ready()
    {
        base._Ready();
        Instance = this;
    }

    public async Task Slide(TextureRect slideRect, Tween tween, double duration, 
        bool slideX, bool slideY, bool flipX, bool flipY)
    {
        tween.SetEase(Tween.EaseType.InOut);

        var texture = ImageTexture.CreateFromImage(util.Util.Instance.Screenshot());
        var screenSize = DisplayServer.WindowGetSize();
        
        slideRect.SetPosition(Vector2.Zero);
        slideRect.Texture = texture;
        slideRect.SetSize(screenSize);

        var finalPosition = new Vector2(slideX ? screenSize.X : 0, slideY ? screenSize.Y : 0);
        finalPosition.X = flipX ? -finalPosition.X: finalPosition.X;
        finalPosition.Y = flipY ? -finalPosition.Y: finalPosition.Y;

        tween.TweenProperty(slideRect, "position", finalPosition, duration);

        await ToSignal(tween, Tween.SignalName.Finished);
    }

    public async Task Fade(ColorRect fadeRect, Tween tween, double duration, Color from, Color to)
    {
        fadeRect.Modulate = from;
        tween.TweenProperty(fadeRect, "modulate", to, duration);
        await ToSignal(tween, Tween.SignalName.Finished);
    }

    public async Task Zoom(TextureRect zoomRect, Tween tween, double duration)
    {
        // TODO does not center on the screen
        var texture = ImageTexture.CreateFromImage(util.Util.Instance.Screenshot());
        var screenSize = DisplayServer.WindowGetSize();

        zoomRect.SetPosition(Vector2.Zero);
        zoomRect.Scale = Vector2.One;
        zoomRect.Texture = texture;
        zoomRect.SetSize(screenSize);

        tween.TweenProperty(zoomRect, "scale", Vector2.Zero, duration);
        
        await ToSignal(tween, Tween.SignalName.Finished);
    }
    
    public async Task Dissolve(TextureRect dissolveRect, Tween tween, double duration)
    {
        var texture = ImageTexture.CreateFromImage(util.Util.Instance.Screenshot());
        var screenSize = DisplayServer.WindowGetSize();

        dissolveRect.Texture = texture;
        dissolveRect.SetSize(screenSize);
        
        dissolveRect.Material.Set("shader_parameter/dissolve_state", 0.0);
        tween.TweenProperty(dissolveRect.Material, "shader_parameter/dissolve_state", 1.1, duration);
        
        await ToSignal(tween, Tween.SignalName.Finished);
    }

    public async Task Pixelate(TextureRect pixelateRect, Tween tween, double duration)
    {
        await Pixelate(pixelateRect, tween, duration, false);
    }

    public async Task Pixelate(TextureRect pixelateRect, Tween tween, double duration, bool backwards)
    {
        // TODO Does not work as intended
        var texture = ImageTexture.CreateFromImage(util.Util.Instance.Screenshot());
        var screenSize = DisplayServer.WindowGetSize();

        pixelateRect.Texture = texture;
        pixelateRect.SetSize(screenSize);

        pixelateRect.Visible = true;
        pixelateRect.Material.Set("shader_parameter/pixelFactor", backwards ? 0.0 : 0.5);
        tween.TweenProperty(pixelateRect.Material, "shader_parameter/pixelFactor", backwards ? 0.5 : 0.0, duration);
        
        await ToSignal(tween, Tween.SignalName.Finished);
    }
}