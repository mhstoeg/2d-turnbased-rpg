using System;
using System.Threading.Tasks;
using Godot;
using TurnBasedRPG.entities.settings.audio;
using AudioSettings = TurnBasedRPG.entities.settings.audio.AudioSettings;
using SoundManager = TurnBasedRPG.entities.sound.sound_manager.SoundManager;

namespace TurnBasedRPG.entities.scene_switcher;


public partial class SceneSwitcher : CanvasLayer
{
	
	public static SceneSwitcher Instance { get; private set; }
	private ColorRect FadeRect { set; get; }
	private TextureRect TextureRect { set; get; }
	private TextureRect DissolveRect { set; get; }
	private TextureRect PixelateRect { set; get; }

	public const float TransitionTime = 0.5f;
	

	[Signal] public delegate void SceneSwitchedEventHandler();

	public override void _Ready()
	{
		base._Ready();
		Instance = this;

		FadeRect = GetNode<ColorRect>("FadeRect");
		TextureRect = GetNode<TextureRect>("TextureRect");
		DissolveRect = GetNode<TextureRect>("DissolveRect");
		PixelateRect = GetNode<TextureRect>("PixelateRect");
	}


	public async Task<Node> SwitchScene(Node parent, PackedScene scene, Node nodeToFree,  SceneTransition transition)
	{
		var alreadyPaused = GetTree().Paused;

		if (!alreadyPaused)
			GetTree().Paused = true;
		
		await PlayTransition(transition, TransitionTime);

		// TODO anything that needs to be reset should be called between thing animation and the scene change
		// TODO for instance call the ResetManager

		var instance = scene.Instantiate();
		parent.AddChild(instance);

		nodeToFree?.QueueFree();

		await PlayTransition(transition, TransitionTime, true);
		
		if (!alreadyPaused)
			GetTree().Paused = false;

		return instance;
	}

	private async Task PlayTransition(SceneTransition transition, float duration)
	{
		await PlayTransition(transition, duration, false);
	}

	private async Task PlayTransition(SceneTransition transition, float duration, bool backwards)
	{
		// TODO Clean this function up
		var tween = CreateTween().SetParallel();
		var settingName = AudioSettingType.MusicVol.ToString();
		var currentMusicVolDb = AudioSettings.Instance.GetSetting<float>(settingName);
		switch (transition)
		{
			case SceneTransition.Fade:
				if (backwards) {
					SoundManager.Instance.FadeMusic(tween, 0, currentMusicVolDb);
					await TransitionAnimation.Instance.Fade(FadeRect, tween, duration, 
					Colors.Black, new Color(0,0,0,0));
				}
				else
				{
					SoundManager.Instance.FadeMusic(tween, duration, -50);
					await TransitionAnimation.Instance.Fade(FadeRect, tween, duration, 
					new Color(0,0,0,0), Colors.Black);
				}
				break;
			case SceneTransition.Slide:
				if (backwards) await TransitionAnimation.Instance.Slide(TextureRect, tween, duration,
					true, false, false, false);
				else tween.Kill();
				break;
			case SceneTransition.Zoom:
				if (backwards) await TransitionAnimation.Instance.Zoom(TextureRect, tween, duration);
				else tween.Kill();
				break;
			case SceneTransition.Dissolve:
				if (backwards)
				{
					SoundManager.Instance.FadeMusic(tween, duration, currentMusicVolDb);
					await TransitionAnimation.Instance.Dissolve(DissolveRect, tween, duration);
				}
				else SoundManager.Instance.FadeMusic(tween, duration, -50);
				break;
			case SceneTransition.Pixelate:
				if (backwards) await TransitionAnimation.Instance.Pixelate(PixelateRect, tween,
					duration, true);
				else tween.Kill();
				break;
			case SceneTransition.None:
				tween.Kill();
				break;
			default:
				throw new NotImplementedException("SceneTransition " + transition + " is not implemented");
		}
	}
	
	
}