﻿namespace TurnBasedRPG.entities.scene_switcher;

public enum SceneTransition
{
	Fade,
	Slide,
	Zoom,
	Dissolve,
	Pixelate,
	None
}