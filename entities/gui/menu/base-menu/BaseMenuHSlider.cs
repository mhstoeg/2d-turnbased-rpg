using Godot;
using TurnBasedRPG.entities.sound.sound_manager;

namespace TurnBasedRPG.entities.gui.menu.base_menu;

public partial class BaseMenuHSlider : HSlider
{
    public override void _Ready()
    {
        base._Ready();
        Connect(Control.SignalName.FocusEntered, new Callable(this, MethodName.OnSliderEntered));
        Connect(Control.SignalName.MouseEntered, new Callable(this, MethodName.OnSliderEntered));
    }

    private void OnSliderEntered()
    {
        SoundManager.Instance.PlaySoundQueue(SoundManagerQueueSound.BtnEnter);
    }
    
}