using Godot;
using TurnBasedRPG.entities.sound.sound_manager;

namespace TurnBasedRPG.entities.gui.menu.base_menu;

public partial class BaseGameOptionButton : OptionButton
{

    public override void _Ready()
    {
        base._Ready();
        Connect(Control.SignalName.FocusEntered, new Callable(this, MethodName.OnBtnEntered));
        Connect(Control.SignalName.MouseEntered, new Callable(this, MethodName.OnBtnEntered));
        
        Connect(OptionButton.SignalName.ItemFocused, new Callable(this, MethodName.OnItemEntered));
        Connect(OptionButton.SignalName.ItemSelected, new Callable(this, MethodName.OnItemEntered));
    }

    private void OnBtnEntered()
    {
        if (!Disabled)
            SoundManager.Instance.PlaySoundQueue(SoundManagerQueueSound.BtnEnter);
    }
    
    private void OnItemEntered()
    {
        if (!Disabled)
            SoundManager.Instance.PlaySound(SoundManagerSound.BtnClick);
    }
}