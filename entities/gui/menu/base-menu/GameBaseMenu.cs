using System.Threading.Tasks;
using Godot;
using TurnBasedRPG.entities.scene_switcher;
using BaseStateMachine = TurnBasedRPG.entities.state.BaseStateMachine;
using SceneSwitcher = TurnBasedRPG.entities.scene_switcher.SceneSwitcher;

namespace TurnBasedRPG.entities.gui.menu.base_menu;

public abstract partial class GameBaseMenu : Control
{
	public Control MenuComponents { get; private set; }
	public BaseStateMachine StateMachine { get; private set; }
	protected PackedScene SubMenuScene { get; set; }
	public Control SubMenu { get; set; }
	
	public override void _Ready()
	{
		MenuComponents = GetNode<Control>("MenuComponents");
		StateMachine = GetNode<BaseStateMachine>("StateMachine");
		
		StateMachine.State = BaseMenuState.BaseMenu;
	}

	private void ReturnToBaseMenu()
	{
		StateMachine.State = BaseMenuState.BaseMenu;
	}

	public async Task OpenSubMenu()
	{
		SubMenu = (Control) await SceneSwitcher.Instance.SwitchScene(this, SubMenuScene, 
			null, SceneTransition.None);
		SubMenu.Connect(Node.SignalName.TreeExited, new Callable(this, MethodName.ReturnToBaseMenu));
	}
	

}