using Godot;
using TurnBasedRPG.entities.sound.sound_manager;

namespace TurnBasedRPG.entities.gui.menu.base_menu;

public partial class BaseGameCheckButton : CheckButton
{

    public override void _Ready()
    {
        base._Ready();
        Connect(Control.SignalName.FocusEntered, new Callable(this, MethodName.OnBtnEntered));
        Connect(Control.SignalName.MouseEntered, new Callable(this, MethodName.OnBtnEntered));
        
    }

    private void OnBtnEntered()
    {
        if (!Disabled)
            SoundManager.Instance.PlaySoundQueue(SoundManagerQueueSound.BtnEnter);
    }
}