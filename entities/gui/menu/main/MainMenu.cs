using Godot;
using TurnBasedRPG.entities.gui.menu.base_menu;
using TurnBasedRPG.entities.sound.sound_manager;
using SoundManager = TurnBasedRPG.entities.sound.sound_manager.SoundManager;

namespace TurnBasedRPG.entities.gui.menu.main;


public partial class MainMenu : GameBaseMenu
{
    
    private PackedScene OptionsMenuScene { set; get; }

    [Signal] public delegate void GameStartEventHandler();

    public override void _Ready()
    {
        base._Ready();
        OptionsMenuScene = ResourceLoader.Load<PackedScene>("res://entities/gui/menu/options/OptionsMenu.tscn");
		SoundManager.Instance.SetMusic(
			ResourceLoader.Load<AudioStream>("res://assets/audio/music/the-white-lion-10379.mp3"));
    }

    private void OnStartBtnPressed()
    {
        SoundManager.Instance.PlaySound(SoundManagerSound.BtnClick);
        StateMachine.State = MainMenuState.Start;
    }

    private void OnOptionBtnPressed()
    {
        SoundManager.Instance.PlaySound(SoundManagerSound.BtnClick);
        SubMenuScene = OptionsMenuScene;
        StateMachine.State = BaseMenuState.SubMenu;
    }

    private void OnQuitBtnPressed()
    {
        StateMachine.State = MainMenuState.Quit;
    }

 
}