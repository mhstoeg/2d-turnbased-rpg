using System;
using TurnBasedRPG.entities.gui.menu.base_menu;
using TurnBasedRPG.entities.sound.sound_manager;
using TurnBasedRPG.entities.state;
using SoundManager = TurnBasedRPG.entities.sound.sound_manager.SoundManager;

namespace TurnBasedRPG.entities.gui.menu.main;



public partial class MainMenuStateMachine : BaseStateMachine
{
	private MainMenu MainMenu { set; get; }
	public override void _Ready()
	{
		MainMenu = GetNode<MainMenu>("..");
	}


	protected override void StateLogic(double delta)
	{
		switch (State)
		{
			case BaseMenuState.BaseMenu:
				break;
			case BaseMenuState.SubMenu:
				break;
			case MainMenuState.Start:
				break;
			case MainMenuState.Quit:
				break;
		}
	}

	protected override Enum GetTransition(double delta)
	
	{
		var transition = base.GetTransition(delta);
		switch (State)
		{
			case BaseMenuState.BaseMenu:
				break;
			case BaseMenuState.SubMenu:
				break;
			case MainMenuState.Start:
				break;
			case MainMenuState.Quit:
				break;
		}

		return transition;
	}

	protected override async void EnterState(Enum newState, Enum oldState)
	{
		switch (newState)
		{
			case BaseMenuState.BaseMenu:
				MainMenu.MenuComponents.Show();
				break;
			case BaseMenuState.SubMenu:
				await MainMenu.OpenSubMenu();
				break;
			case MainMenuState.Start:
				SoundManager.Instance.PlaySoundQueue(SoundManagerQueueSound.GameStart);
				MainMenu.EmitSignal(MainMenu.SignalName.GameStart);
				break;
			case MainMenuState.Quit:
				await SoundManager.Instance.PlaySound(SoundManagerSound.BtnClick, true);
				GetTree().Quit();
				break;
			default:
				throw new Exception("Entered invalid State, " + State);
		}
	}

	protected override void ExitState(Enum oldState, Enum newState)
	{
		switch (oldState)
		{
			case BaseMenuState.BaseMenu:
				MainMenu.MenuComponents.Hide();
				break;
			case BaseMenuState.SubMenu:
				break;
			case MainMenuState.Start:
				break;
			case MainMenuState.Quit:
				break;
			default:
				throw new Exception("Exited invalid State, " + State);
		}
	}
}