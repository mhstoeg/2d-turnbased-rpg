﻿namespace TurnBasedRPG.entities.gui.menu.main;

public enum MainMenuState
{
    Start,
    Quit
}