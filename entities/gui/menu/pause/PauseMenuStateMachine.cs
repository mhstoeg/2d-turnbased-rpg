using System;
using TurnBasedRPG.entities.gui.menu.base_menu;
using TurnBasedRPG.entities.state;

namespace TurnBasedRPG.entities.gui.menu.pause;



public partial class PauseMenuStateMachine : BaseStateMachine
{
	private PauseMenu PauseMenu{ set; get; }
	public override void _Ready()
	{
		PauseMenu = GetNode<PauseMenu>("..");
	}


	protected override void StateLogic(double delta)
	{
		switch (State)
		{
			case BaseMenuState.BaseMenu:
				break;
			case BaseMenuState.SubMenu:
				break;
			case PauseMenuState.Hidden:
				break;
			case PauseMenuState.Exit:
				break;
		}
	}

	protected override Enum GetTransition(double delta)
	
	{
		var transition = base.GetTransition(delta);
		switch (State)
		{
			case BaseMenuState.BaseMenu:
				break;
			case BaseMenuState.SubMenu:
				break;
			case PauseMenuState.Hidden:
				break;
			case PauseMenuState.Exit:
				break;

		}
		return transition;
	}

	protected override async void EnterState(Enum newState, Enum oldState)
	{
		switch (newState)
		{
			case BaseMenuState.BaseMenu:
				PauseMenu.Show();
				PauseMenu.MenuComponents.Show();
				GetTree().Paused = true;
				break;
			case BaseMenuState.SubMenu:
				await PauseMenu.OpenSubMenu();
				break;
			case PauseMenuState.Hidden:
				GetTree().Paused = false;
				break;
			case PauseMenuState.Exit:
				GetTree().Paused = false;
				PauseMenu.EmitSignal(PauseMenu.SignalName.GoToMain);
				break;
			default:
				throw new Exception("Entered invalid State, " + State);
		}
	}

	protected override void ExitState(Enum oldState, Enum newState)
	{
		switch (oldState)
		{
			case BaseMenuState.BaseMenu:
				PauseMenu.MenuComponents.Hide();
				break;
			case BaseMenuState.SubMenu:
				PauseMenu.SubMenu = null;
				break;
			case PauseMenuState.Hidden:
				PauseMenu.Hide();
				break;
			case PauseMenuState.Exit:
				break;
			default:
				throw new Exception("Exited invalid State, " + State);
		}
	}
}