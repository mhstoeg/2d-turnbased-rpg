﻿namespace TurnBasedRPG.entities.gui.menu.pause;

public enum PauseMenuState
{
    Hidden,
    Exit 
}