using Godot;
using TurnBasedRPG.entities.gui.menu.base_menu;
using TurnBasedRPG.entities.sound.sound_manager;
using SoundManager = TurnBasedRPG.entities.sound.sound_manager.SoundManager;

namespace TurnBasedRPG.entities.gui.menu.pause;

public partial class PauseMenu : GameBaseMenu
{
    
    private PackedScene OptionsMenuScene { set; get; }

    [Signal] public delegate void GoToMainEventHandler();

    public override void _Ready()
    {
        base._Ready();
        OptionsMenuScene = ResourceLoader.Load<PackedScene>("res://entities/gui/menu/options/OptionsMenu.tscn");

        StateMachine.State = PauseMenuState.Hidden;
    }


    public override void _Input(InputEvent @event)
    {
        if (StateMachine.IsState(BaseMenuState.SubMenu)) return;
        if (!@event.IsActionPressed("ui_cancel")) return;

        SoundManager.Instance.PlaySound(SoundManagerSound.BtnClick);
        
        if (StateMachine.IsState(PauseMenuState.Hidden))
            StateMachine.State = BaseMenuState.BaseMenu;
        else if (StateMachine.IsState(BaseMenuState.BaseMenu))
            StateMachine.State = PauseMenuState.Hidden;
    }

    private void OnResumePressed()
    {
        SoundManager.Instance.PlaySound(SoundManagerSound.BtnClick);
        StateMachine.State = PauseMenuState.Hidden;
    }

    private void OnOptionsBtnPressed()
    {
        SoundManager.Instance.PlaySound(SoundManagerSound.BtnClick);
        SubMenuScene = OptionsMenuScene;
        StateMachine.State = BaseMenuState.SubMenu;
    }

    private void OnExitBtnPressed()
    {
        SoundManager.Instance.PlaySound(SoundManagerSound.BtnClick);
        StateMachine.State = PauseMenuState.Exit;
        
    }
}