using Godot;
using TurnBasedRPG.entities.settings.video;
using VideoSettings = TurnBasedRPG.entities.settings.video.VideoSettings;

namespace TurnBasedRPG.entities.gui.menu.options.tabs.settings.video;

public partial class DisplayOptionSetting : BaseSetting
{
	
	private OptionButton DisplayOptionsBtn { set; get; }

	public override void _Ready()
	{
		base._Ready();
		DisplayOptionsBtn = GetNode<OptionButton>("DisplayOptionsBtn");
		DisplayOptionsBtn.Connect(OptionButton.SignalName.ItemSelected, 
			new Callable(this, MethodName.OnItemSelected));
	}

	public override void Ready()
	{
		
	}
	
	public override void UpdateGui()
	{
		var settingName = VideoSettingType.DisplayOption.ToString();
		DisplayOptionsBtn.Select(VideoSettings.Instance.GetSetting<int>(settingName));
	}

	
	private void OnItemSelected(long index)
	{
		var settingName = VideoSettingType.DisplayOption.ToString();
		if (index == VideoSettings.Instance.GetSetting<int>(settingName)) return;
		VideoSettings.Instance.SetSetting(settingName, index);
		EmitSignal(entities.gui.menu.options.tabs.settings.BaseSetting.SignalName.SettingChanged, false);
	}

}
