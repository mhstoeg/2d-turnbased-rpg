using System;
using System.Globalization;
using Godot;
using TurnBasedRPG.entities.settings.video;
using Range = Godot.Range;
using VideoSettings = TurnBasedRPG.entities.settings.video.VideoSettings;
using WindowManager = TurnBasedRPG.entities.window_manager.WindowManager;

namespace TurnBasedRPG.entities.gui.menu.options.tabs.settings.video;

public partial class MaxFpsSetting: BaseSetting
{
	
	private HSlider MaxFpsSlider { set; get; }
	private Label MaxFpsValLabel { set; get; }
	private const double SliderStep = 1;

	public override void _Ready()
	{
		base._Ready();
		MaxFpsSlider = GetNode<HSlider>("MaxFpsContainer/MaxFpsSlider");
		MaxFpsValLabel = GetNode<Label>("MaxFpsContainer/MaxFpsValLabel");
		MaxFpsSlider.Connect(Range.SignalName.ValueChanged, new Callable(this, MethodName.OnSliderValueChanged));
	}

	public override void Ready()
	{
		var settingName = VideoSettingType.MaxFps.ToString();
		var currentValue = VideoSettings.Instance.GetSetting<int>(settingName);
		
		MaxFpsSlider.SetValueNoSignal(WindowManager.MinFpsMax);
		MaxFpsSlider.MinValue = WindowManager.MinFpsMax;
		MaxFpsSlider.MaxValue = WindowManager.MaxFpsMax;
		MaxFpsSlider.Step = SliderStep;
		
		VideoSettings.Instance.SetSetting(settingName, currentValue);
	}
	
	public override void UpdateGui()
	{
		UpdateSlider();
		UpdateLabel();
	}

	private void UpdateSlider()
	{
		var settingName = VideoSettingType.MaxFps.ToString();
		var maxFps = VideoSettings.Instance.GetSetting<int>(settingName);
		MaxFpsSlider.Value = maxFps > 0 && maxFps < MaxFpsSlider.MaxValue ? maxFps : MaxFpsSlider.MaxValue;
	}

	private void UpdateLabel()
	{
		var settingName = VideoSettingType.MaxFps.ToString();
		var maxFps = VideoSettings.Instance.GetSetting<int>(settingName);
		MaxFpsValLabel.Text = maxFps > 0 && maxFps < MaxFpsSlider.MaxValue 
			? MaxFpsSlider.Value.ToString(CultureInfo.InvariantCulture) : "Unlimited";
	}

	private void OnSliderValueChanged(float value)
	{
		var settingName = VideoSettingType.MaxFps.ToString();
		var currentValue = VideoSettings.Instance.GetSetting<int>(settingName);
		var sliderDifference = Math.Abs(value - currentValue);
		
		if (!(sliderDifference >= SliderStep)) return;
		value = value < MaxFpsSlider.MaxValue ? value : 0;
		VideoSettings.Instance.SetSetting(settingName, value);
		UpdateLabel();
		EmitSignal(entities.gui.menu.options.tabs.settings.BaseSetting.SignalName.SettingChanged, false);

	}

}
