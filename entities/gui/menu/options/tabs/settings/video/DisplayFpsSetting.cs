using Godot;
using TurnBasedRPG.entities.settings.video;
using VideoSettings = TurnBasedRPG.entities.settings.video.VideoSettings;

namespace TurnBasedRPG.entities.gui.menu.options.tabs.settings.video;

public partial class DisplayFpsSetting: BaseSetting
{
	
	private CheckButton DisplayFpsBtn { set; get; }

	public override void _Ready()
	{
		base._Ready();
		DisplayFpsBtn = GetNode<CheckButton>("DisplayFpsBtn");
		DisplayFpsBtn.Connect(BaseButton.SignalName.Toggled, new Callable(this, MethodName.OnButtonToggled));
	}

	public override void Ready()
	{
	}
	
	public override void UpdateGui()
	{
		var settingName = VideoSettingType.DisplayFps.ToString();
		DisplayFpsBtn.ButtonPressed = VideoSettings.Instance.GetSetting<bool>(settingName);
	}

	private void OnButtonToggled(bool buttonPressed)
	{
		var settingName = VideoSettingType.DisplayFps.ToString();
		if (buttonPressed == VideoSettings.Instance.GetSetting<bool>(settingName)) return;
		VideoSettings.Instance.SetSetting(settingName, buttonPressed);
		EmitSignal(entities.gui.menu.options.tabs.settings.BaseSetting.SignalName.SettingChanged, false);
	}

}
