using Godot;
using TurnBasedRPG.entities.settings.video;
using VideoSettings = TurnBasedRPG.entities.settings.video.VideoSettings;

namespace TurnBasedRPG.entities.gui.menu.options.tabs.settings.video;

public partial class MonitorOptionSetting: BaseSetting
{
	
	private OptionButton MonitorOptionsBtn { set; get; }

	public override void _Ready()
	{
		base._Ready();
		MonitorOptionsBtn = GetNode<OptionButton>("MonitorOptionsBtn");
		MonitorOptionsBtn.Connect(OptionButton.SignalName.ItemSelected, new Callable(this, MethodName.OnItemSelected));
	}

	public override void Ready()
	{
		for (var i = 0; i < DisplayServer.GetScreenCount(); i++)
			MonitorOptionsBtn.AddItem("Display " + (i + 1), i);
	}
	
	public override void UpdateGui()
	{
		var settingName = VideoSettingType.MonitorOption.ToString();
		MonitorOptionsBtn.Select(VideoSettings.Instance.GetSetting<int>(settingName));
	}

	private void OnItemSelected(int index)
	{
		var settingName = VideoSettingType.MonitorOption.ToString();
		if (index == VideoSettings.Instance.GetSetting<int>(settingName)) return;
		VideoSettings.Instance.SetSetting(settingName, index);
		EmitSignal(entities.gui.menu.options.tabs.settings.BaseSetting.SignalName.SettingChanged, false);
	}

}
