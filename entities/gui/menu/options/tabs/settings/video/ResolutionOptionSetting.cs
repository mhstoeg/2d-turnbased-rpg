using Godot;

namespace TurnBasedRPG.entities.gui.menu.options.tabs.settings.video;

public partial class ResolutionOptionSetting: BaseSetting
{
	
	private OptionButton ResolutionOptionsBtn { set; get; }

	public override void _Ready()
	{
		base._Ready();
		ResolutionOptionsBtn = GetNode<OptionButton>("ResolutionOptionsBtn");
	}

	public override void Ready()
	{
	}
	
	public override void UpdateGui()
	{
	}

}
