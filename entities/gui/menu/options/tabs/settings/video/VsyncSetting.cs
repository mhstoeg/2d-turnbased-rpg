using System;
using Godot;
using TurnBasedRPG.entities.settings.video;
using VideoSettings = TurnBasedRPG.entities.settings.video.VideoSettings;

namespace TurnBasedRPG.entities.gui.menu.options.tabs.settings.video;

public partial class VsyncSetting: BaseSetting
{
	
	private CheckButton VsyncBtn { set; get; }

	public override void _Ready()
	{
		base._Ready();
		VsyncBtn = GetNode<CheckButton>("VsyncCheckBtn");
		VsyncBtn.Connect(BaseButton.SignalName.Toggled, new Callable(this, MethodName.OnButtonToggled));
	}

	public override void Ready()
	{
	}
	
	public override void UpdateGui()
	{
		var settingName = VideoSettingType.Vsync.ToString();
		VsyncBtn.ButtonPressed = VideoSettings.Instance.GetSetting<bool>(settingName);
	}

	private void OnButtonToggled (bool buttonPressed)
	{
		var settingName = VideoSettingType.Vsync.ToString();
		var vsyncSetting = VideoSettings.Instance.GetSetting<bool>(settingName);
		if (buttonPressed == vsyncSetting) return;
		VideoSettings.Instance.SetSetting(settingName, Convert.ToSingle(buttonPressed));
		EmitSignal(entities.gui.menu.options.tabs.settings.BaseSetting.SignalName.SettingChanged, false);
	}

}
