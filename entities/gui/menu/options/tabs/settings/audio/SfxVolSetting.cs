using System;
using Godot;
using TurnBasedRPG.entities.settings.audio;
using AudioSettings = TurnBasedRPG.entities.settings.audio.AudioSettings;
using Range = Godot.Range;

namespace TurnBasedRPG.entities.gui.menu.options.tabs.settings.audio;

public partial class SfxVolSetting : BaseSetting
{
	
	private HSlider SfxVolSlider{ set; get; }
	private const double SliderStep = 1.0;

	public override void _Ready()
	{
		base._Ready();
		SfxVolSlider = GetNode<HSlider>("SfxVolSlider");
		SfxVolSlider.Connect(Range.SignalName.ValueChanged, new Callable(this, MethodName.OnSliderValueChanged));
	}

	public override void Ready()
	{
		var settingName = AudioSettingType.SfxVol.ToString();
		var currentValue = AudioSettings.Instance.GetSetting<int>(settingName);
		
		SfxVolSlider.SetValueNoSignal(AudioSettings.MinVol);
		SfxVolSlider.MinValue = AudioSettings.MinVol;
		SfxVolSlider.MaxValue = AudioSettings.MaxVol;
		SfxVolSlider.Step = SliderStep;
		
		AudioSettings.Instance.SetSetting(settingName, currentValue);
	}
	
	public override void UpdateGui()
	{
		var settingName = AudioSettingType.SfxVol.ToString();
		SfxVolSlider.Value = AudioSettings.Instance.GetSetting<double>(settingName);
	}


	private void OnSliderValueChanged(float value)
	{
		var settingName = AudioSettingType.SfxVol.ToString();
		var currentValue = AudioSettings.Instance.GetSetting<int>(settingName);
		var sliderDifference = Math.Abs(value - currentValue);
		
		if (!(sliderDifference >= SliderStep)) return;
		AudioSettings.Instance.SetSetting(settingName, value);
		EmitSignal(entities.gui.menu.options.tabs.settings.BaseSetting.SignalName.SettingChanged, false);
	}
}
