using System;
using Godot;
using TurnBasedRPG.entities.settings.audio;
using AudioSettings = TurnBasedRPG.entities.settings.audio.AudioSettings;
using Range = Godot.Range;

namespace TurnBasedRPG.entities.gui.menu.options.tabs.settings.audio;

public partial class MusicVolSetting : BaseSetting
{
	
	private HSlider MusicVolSlider{ set; get; }
	private const double SliderStep = 1.0;

	public override void _Ready()
	{
		base._Ready();
		MusicVolSlider = GetNode<HSlider>("MusicVolSlider");
		MusicVolSlider.Connect(Range.SignalName.ValueChanged, new Callable(this, MethodName.OnSliderValueChanged));
	}

	public override void Ready()
	{
		var settingName = AudioSettingType.MusicVol.ToString();
		var currentValue = AudioSettings.Instance.GetSetting<int>(settingName);
		
		MusicVolSlider.SetValueNoSignal(AudioSettings.MinVol);
		MusicVolSlider.MinValue = AudioSettings.MinVol;
		MusicVolSlider.MaxValue = AudioSettings.MaxVol;
		MusicVolSlider.Step = SliderStep;
		
		AudioSettings.Instance.SetSetting(settingName, currentValue);
	}
	
	public override void UpdateGui()
	{
		var settingName = AudioSettingType.MusicVol.ToString();
		MusicVolSlider.Value = AudioSettings.Instance.GetSetting<double>(settingName);
	}


	private void OnSliderValueChanged(float value)
	{
		var settingName = AudioSettingType.MusicVol.ToString();
		var currentValue = AudioSettings.Instance.GetSetting<int>(settingName);
		var sliderDifference = Math.Abs(value - currentValue);
		
		if (!(sliderDifference>= SliderStep)) return;
		AudioSettings.Instance.SetSetting(settingName, value);
		EmitSignal(entities.gui.menu.options.tabs.settings.BaseSetting.SignalName.SettingChanged, false);
	}
}
