using System;
using System.Globalization;
using Godot;
using TurnBasedRPG.entities.settings.gameplay;
using TurnBasedRPG.util;
using GameplaySettings = TurnBasedRPG.entities.settings.gameplay.GameplaySettings;
using Range = Godot.Range;

namespace TurnBasedRPG.entities.gui.menu.options.tabs.settings.gameplay;

public partial class MouseSensSetting : BaseSetting
{
	
	private HSlider MouseSensSlider { set; get; }
	private Label MouseSensValLabel { set; get; }
	private const float SliderStep = 0.01f;

	public override void _Ready()
	{
		
		base._Ready();
		MouseSensSlider = GetNode<HSlider>("MouseSensContainer/MouseSensSlider");
		MouseSensValLabel = GetNode<Label>("MouseSensContainer/MouseSensValLabel");
		MouseSensSlider.Connect(Range.SignalName.ValueChanged, new Callable(this, MethodName.OnSliderValueChanged));
	}

	public override void Ready()
	{
		var settingName = GameplaySettingType.MouseSens.ToString();
		var currentValue = GameplaySettings.Instance.GetSetting<float>(settingName);
		
		MouseSensSlider.SetValueNoSignal(GameplaySettings.MinMouseSens);
		MouseSensSlider.MinValue = GameplaySettings.MinMouseSens;
		MouseSensSlider.MaxValue = GameplaySettings.MaxMouseSens;
		MouseSensSlider.Step = SliderStep;
		
		// Set setting since changing the min value of slider calls OnSliderValueChanged and sets it to MinMouseSens
		GameplaySettings.Instance.SetSetting(settingName, currentValue);
	}
	
	public override void UpdateGui()
	{
		UpdateSlider();
		UpdateLabel();
	}

	private void UpdateSlider()
	{
		var settingName = GameplaySettingType.MouseSens.ToString();
		MouseSensSlider.Value = GameplaySettings.Instance.GetSetting<float>(settingName);
	}

	private void UpdateLabel()
	{
		var settingName = GameplaySettingType.MouseSens.ToString();
		MouseSensValLabel.Text = ( 
			GameplaySettings.Instance.GetSetting<float>(settingName)).ToString(CultureInfo.InvariantCulture);
	}

	private void OnSliderValueChanged(float value)
	{
		var settingName = GameplaySettingType.MouseSens.ToString();
		var decimalPlace = Calculate.DecimalPlace(SliderStep);
		value = (float) Math.Round(value, decimalPlace);
		var currentValue = GameplaySettings.Instance.GetSetting<float>(settingName);
		var sliderDifference = Math.Abs(value - currentValue);
		
		if (!(sliderDifference >= SliderStep)) return;
		GameplaySettings.Instance.SetSetting(settingName, value);
		UpdateLabel();
		EmitSignal(entities.gui.menu.options.tabs.settings.BaseSetting.SignalName.SettingChanged, false);

	}

}
