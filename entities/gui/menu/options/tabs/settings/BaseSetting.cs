using Godot;

namespace TurnBasedRPG.entities.gui.menu.options.tabs.settings;

public abstract partial class BaseSetting : Node
{

	
    [Signal] public delegate void SettingChangedEventHandler();
    
	public abstract void UpdateGui();
	public new abstract void Ready();


}