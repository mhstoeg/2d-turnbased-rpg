using BaseSetting = TurnBasedRPG.entities.gui.menu.options.tabs.settings.BaseSetting;

namespace TurnBasedRPG.entities.gui.menu.options.tabs;

public partial class VideoTab : BaseTabBar
{
	public override void _Ready()
	{

		base._Ready();
		
		const string intermediaryPath = "MarginContainer/VideoSettings/";
		
		Settings = new []
		{
			GetNode<BaseSetting>(intermediaryPath + "DisplayOption"),
			GetNode<BaseSetting>(intermediaryPath + "Resolution"),
			GetNode<BaseSetting>(intermediaryPath + "Monitor"),
			GetNode<BaseSetting>(intermediaryPath + "VSync"),
			GetNode<BaseSetting>(intermediaryPath + "DisplayFps"),
			GetNode<BaseSetting>(intermediaryPath + "MaxFps"),
		};
		
		ReadyTab();
		UpdateTabGui();
	}

}