using BaseSetting = TurnBasedRPG.entities.gui.menu.options.tabs.settings.BaseSetting;

namespace TurnBasedRPG.entities.gui.menu.options.tabs;

public partial class AudioTab : BaseTabBar
{
	public override void _Ready()
	{

		base._Ready();
		
		const string intermediaryPath = "MarginContainer/AudioSettings/";
		
		Settings = new []
		{
			GetNode<BaseSetting>(intermediaryPath + "MasterVol"),
			GetNode<BaseSetting>(intermediaryPath + "MusicVol"),
			GetNode<BaseSetting>(intermediaryPath + "SfxVol"),
		};
		
		ReadyTab();
		UpdateTabGui();
	}

}