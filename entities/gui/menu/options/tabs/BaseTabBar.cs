using Godot;
using BaseSetting = TurnBasedRPG.entities.gui.menu.options.tabs.settings.BaseSetting;

namespace TurnBasedRPG.entities.gui.menu.options.tabs;

public partial class BaseTabBar : TabBar
{
	public BaseSetting[] Settings { get; protected set; }
	
	protected void ReadyTab()
	{
		foreach (var setting in Settings)
		{
			setting.Ready();
		}
	    
    }

    public void UpdateTabGui()
    {
		foreach (var setting in Settings)
		{
			setting.UpdateGui();
		}
    }
    
}