using BaseSetting = TurnBasedRPG.entities.gui.menu.options.tabs.settings.BaseSetting;

namespace TurnBasedRPG.entities.gui.menu.options.tabs;

public partial class GameplayTab : BaseTabBar
{
	public override void _Ready()
	{

		base._Ready();
		
		const string intermediaryPath = "MarginContainer/GameplaySettings/";
		
		Settings = new []
		{
			GetNode<BaseSetting>(intermediaryPath + "MouseSens"),
		};
		
		ReadyTab();
		UpdateTabGui();
	}

}