using System.Threading.Tasks;
using Godot;
using Godot.Collections;
using TurnBasedRPG.entities.sound.sound_manager;
using BaseSetting = TurnBasedRPG.entities.gui.menu.options.tabs.settings.BaseSetting;
using BaseTabBar = TurnBasedRPG.entities.gui.menu.options.tabs.BaseTabBar;
using GlobalAnimation = TurnBasedRPG.util.GlobalAnimation;
using SettingsManager = TurnBasedRPG.entities.settings.SettingsManager;
using SoundManager = TurnBasedRPG.entities.sound.sound_manager.SoundManager;

namespace TurnBasedRPG.entities.gui.menu.options;

public partial class OptionsMenu : Control
{
	private Button ReturnBtn { set; get; }
	private Button[] ApplyButtons { set; get; }
	private Button[] RevertButtons { set; get; }
	private Button[] SetDefaultButtons { set; get; }
	private BaseTabBar[] Tabs { set; get; }

	private Dictionary _tempSettings;
	private Dictionary TempSettings { get => _tempSettings.Duplicate(true); set => _tempSettings = value; }


	public override async void _Ready()
	{
		// Return Btn
		ReturnBtn = GetNode<Button>("Top/ReturnButton");
		
		SetupApplyButtons();
		SetupRevertButtons();
		SetupDefaultButtons();
		SetupTabs();
		
		TempSettings = SettingsManager.Instance.GetSettingsCopy();
		ReturnBtn.GrabFocus();
		DisableChangeButtons(true);

		await OpenAnimation();

	}



	private void SetupApplyButtons()
	{
		ApplyButtons = new[]
		{
			GetNode<Button>("SettingsTabs/Video/MarginContainer/VideoSettings/Buttons/ApplyButton"),
			GetNode<Button>("SettingsTabs/Audio/MarginContainer/AudioSettings/Buttons/ApplyButton"),
			GetNode<Button>("SettingsTabs/Gameplay/MarginContainer/GameplaySettings/Buttons/ApplyButton")
		};
		foreach (var btn in ApplyButtons)
		{
			btn.Connect(BaseButton.SignalName.Pressed, new Callable(this, MethodName.OnApplyBtnPressed));
		}
	}

	private void SetupRevertButtons()
	{
		RevertButtons = new[]
		{
			GetNode<Button>("SettingsTabs/Video/MarginContainer/VideoSettings/Buttons/RevertButton"),
			GetNode<Button>("SettingsTabs/Audio/MarginContainer/AudioSettings/Buttons/RevertButton"),
			GetNode<Button>("SettingsTabs/Gameplay/MarginContainer/GameplaySettings/Buttons/RevertButton")
		};
		foreach (var btn in RevertButtons)
		{
			btn.Connect(BaseButton.SignalName.Pressed, new Callable(this, MethodName.OnRevertBtnPressed));
		}
		
	}

	private void SetupDefaultButtons()
	{
		SetDefaultButtons = new[]
		{
			GetNode<Button>("SettingsTabs/Video/MarginContainer/VideoSettings/Buttons/SetDefaultButton"),
			GetNode<Button>("SettingsTabs/Audio/MarginContainer/AudioSettings/Buttons/SetDefaultButton"),
			GetNode<Button>("SettingsTabs/Gameplay/MarginContainer/GameplaySettings/Buttons/SetDefaultButton")
		};
		foreach (var btn in SetDefaultButtons)
		{
			btn.Connect(BaseButton.SignalName.Pressed, new Callable(this, MethodName.OnSetDefaultBtnPressed));
		}
	}

	private void SetupTabs()
	{
		Tabs = new[]
		{
			GetNode<BaseTabBar>("SettingsTabs/Video"),
			GetNode<BaseTabBar>("SettingsTabs/Audio"),
			GetNode<BaseTabBar>("SettingsTabs/Gameplay")
		};
		
		foreach (var tab in Tabs)
		{
			foreach (var setting in tab.Settings)
			{
				setting.Connect(BaseSetting.SignalName.SettingChanged,
					new Callable(this, MethodName.DisableChangeButtons));
			}
		}
	}

	public override async void _Input(InputEvent @event)
	{
		base._Input(@event);
		if (@event.IsActionPressed("ui_cancel"))
			await ReturnMenu();
	}

	private async Task OpenAnimation()
	{
		var tween = CreateTween();
		tween.SetTrans(Tween.TransitionType.Expo);
		await GlobalAnimation.Instance.Slide(this, tween, 0.2, true, false, 
			true, false, false);
	}

	private async Task CloseAnimation()
	{
		var tween = CreateTween();
		tween.SetTrans(Tween.TransitionType.Expo);
		await GlobalAnimation.Instance.Slide(this, tween, 0.2, false, false, 
			true, false, false);
	}

	private async Task ReturnMenu()
	{
		SoundManager.Instance.PlaySound(SoundManagerSound.BtnClick);
        // Restore settings state if not applied
        SettingsManager.Instance.SetToUserSettings(TempSettings);
        await CloseAnimation();
        QueueFree();
	}

	private async void OnReturnBtnPressed()
	{
		await ReturnMenu();
	}

	private void OnRevertBtnPressed()
	{
		SoundManager.Instance.PlaySound(SoundManagerSound.BtnClick);
        SettingsManager.Instance.SetToUserSettings(TempSettings);
		DisableChangeButtons(true);
		UpdateOptionsMenu();
	}

	private void OnSetDefaultBtnPressed()
	{
		SoundManager.Instance.PlaySound(SoundManagerSound.BtnClick);
		DisableChangeButtons(false);
		SettingsManager.Instance.SetToDefault();
		UpdateOptionsMenu();
	}
	
	private void OnApplyBtnPressed()
	{
		SoundManager.Instance.PlaySound(SoundManagerSound.BtnClick);
		DisableChangeButtons(true);
		SettingsManager.Instance.ApplySettings();
		TempSettings = SettingsManager.Instance.GetSettingsCopy();
	}
	
	private void DisableChangeButtons(bool disabled)
	{
		foreach (var btn in ApplyButtons)
			btn.Disabled = disabled;
		foreach (var btn in RevertButtons)
			btn.Disabled = disabled;
	}

	private void UpdateOptionsMenu()
	{
		foreach (var tab in Tabs)
			tab.UpdateTabGui();
	}

}