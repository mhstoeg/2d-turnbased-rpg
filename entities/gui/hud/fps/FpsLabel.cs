using Godot;
using VideoSettings = TurnBasedRPG.entities.settings.video.VideoSettings;

namespace TurnBasedRPG.entities.gui.hud.fps;

public partial class FpsLabel :	Label 
{
	private double TimePassed { set; get; }
	private double TimeForOneCall { set; get; }
	
	private const double CallPerSec = 1.0;
	public override void _Ready()
	{
		TimeForOneCall = 1.0 / CallPerSec;
		TimePassed = TimeForOneCall;
		
		VideoSettings.Instance.Connect(VideoSettings.SignalName.FpsDisplayed, 
			new Callable(this, MethodName.OnFpsDisplayed));
	}

	public override void _Process(double delta)
	{
		TimePassed += delta;

		if (!(TimePassed >= TimeForOneCall)) return;
		Text = "FPS: " + Engine.GetFramesPerSecond();
		TimePassed -= TimeForOneCall;
	}

	private void OnFpsDisplayed(bool value)
	{
		if (value) Show(); else Hide();
	}
}