using Godot;
using TurnBasedRPG.entities.settings.video;

namespace TurnBasedRPG.entities.gui.hud;

public partial class Hud : CanvasLayer
{
	private Label FpsLabel { get; set; }
	public override void _Ready()
	{
		FpsLabel = GetNode<Label>("FpsLabel");
		
		VideoSettings.Instance.Connect(VideoSettings.SignalName.FpsDisplayed, 
			new Callable(this, MethodName.OnFpsDisplayed));
		
		var settingName = VideoSettingType.DisplayFps.ToString();
		if (VideoSettings.Instance.GetSetting<bool>(settingName))
			OnFpsDisplayed(true);
	}

	private void OnFpsDisplayed(bool value)
	{
		if (value) FpsLabel.Show(); else FpsLabel.Hide();
	}
}