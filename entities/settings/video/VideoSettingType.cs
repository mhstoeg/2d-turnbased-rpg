﻿namespace TurnBasedRPG.entities.settings.video;

public enum VideoSettingType
{
	DisplayOption,
	ResolutionOption,
	MonitorOption,
	Vsync,
	DisplayFps,
	MaxFps
}