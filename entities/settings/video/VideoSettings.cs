using Godot;
using TurnBasedRPG.entities.window_manager;
using WindowManager = TurnBasedRPG.entities.window_manager.WindowManager;

namespace TurnBasedRPG.entities.settings.video;


public partial class VideoSettings : BaseSettingCategory 
{
	
	public static VideoSettings Instance { get; private set; }
	[Signal] public delegate void FpsDisplayedEventHandler(bool value);
	public override void _Ready()
	{
        base._Ready();
        Instance = this;
		CategoryName = "VIDEO";
		
		DefaultSettings.Add(VideoSettingType.DisplayOption.ToString(), (int) WindowState.Fullscreen);
		DefaultSettings.Add(VideoSettingType.ResolutionOption.ToString(), DisplayServer.GetPrimaryScreen());
		DefaultSettings.Add(VideoSettingType.MonitorOption.ToString(), DisplayServer.GetPrimaryScreen());
		DefaultSettings.Add(VideoSettingType.Vsync.ToString(), 0);
		DefaultSettings.Add(VideoSettingType.DisplayFps.ToString(), false);
		DefaultSettings.Add(VideoSettingType.MaxFps.ToString(), 0);
		
	}

	public override void ApplySettings()
	{
		// TODO Apply changes to only the settings that were actually changed, disable change buttons if they were
		// TODO manually reverted
		// Monitor Option needs to be set before Window Mode when launching the program, otherwise
		// setting window flags do not work correctly as current screen may not yet have the window
		var settingName = VideoSettingType.MonitorOption.ToString();
		WindowManager.Instance.SetMonitor(GetSetting<int>(settingName));
		
		settingName = VideoSettingType.DisplayOption.ToString();
		WindowManager.Instance.SetWindowState(GetSetting<int>(settingName));
		
		settingName = VideoSettingType.Vsync.ToString();
		WindowManager.Instance.SetVsyncMode(GetSetting<int>(settingName));
		
		settingName = VideoSettingType.DisplayFps.ToString();
		SetDisplayFps(GetSetting<bool>(settingName));
		
		settingName = VideoSettingType.MaxFps.ToString();
		WindowManager.Instance.SetMaxFps(GetSetting<int>(settingName));
	}
	
    public void SetDisplayFps(bool displayFps)
    {
        var settingName = VideoSettingType.DisplayFps.ToString();
        EmitSignal(SignalName.FpsDisplayed, displayFps);
        SetSetting(settingName, displayFps);
    }
}