﻿namespace TurnBasedRPG.entities.settings.audio;

public enum AudioSettingType
{
	MasterVol,
	MusicVol,
	SfxVol
}