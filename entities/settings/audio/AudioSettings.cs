using System;
using TurnBasedRPG.entities.sound.sound_manager;
using SoundManager = TurnBasedRPG.entities.sound.sound_manager.SoundManager;

namespace TurnBasedRPG.entities.settings.audio;


public partial class AudioSettings : BaseSettingCategory
{
	public static AudioSettings Instance { get; private set; }
	public const float MinVol = -50f;
	public const float MaxVol = 0f;
	public const float DefaultVol = -10f;
		
	public override void _Ready()
	{
        base._Ready();
        Instance = this;
		CategoryName = "AUDIO";
		
		DefaultSettings.Add(AudioSettingType.MasterVol.ToString(), DefaultVol);
		DefaultSettings.Add(AudioSettingType.MusicVol.ToString(), DefaultVol);
		DefaultSettings.Add(AudioSettingType.SfxVol.ToString(), DefaultVol);
	}


	public override void ApplySettings()
	{
		// TODO Apply changes to only the settings that were actually changed, disable change buttons if they were
		// TODO manually reverted
		var settingName = AudioSettingType.MasterVol.ToString();
		SetMasterVol(GetSetting<float>(settingName));
		
		settingName = AudioSettingType.MusicVol.ToString();
		SetMusicVol(GetSetting<float>(settingName));
		
		settingName = AudioSettingType.SfxVol.ToString();
		SetSfxVol(GetSetting<float>(settingName));
	}


	private void SetMasterVol(float masterVol)
	{
		var settingName = AudioSettingType.MasterVol.ToString();
		var value = masterVol;
		try
		{
			if (value is < MinVol or > MaxVol)
				throw new ArgumentOutOfRangeException();
		}
		catch
		{
			value = GetDefaultSetting<float>(settingName);
		}
		SoundManager.Instance.SetBusVol((int) SoundManagerBus.Master, value);
        SetSetting(settingName, value);
	}


	private void SetMusicVol(float musicVol)
	{
		var settingName = AudioSettingType.MusicVol.ToString();
		var value = musicVol;
		try
		{
			if (value is < MinVol or > MaxVol)
				throw new ArgumentOutOfRangeException();
		}
		catch
		{
			value = GetDefaultSetting<float>(settingName);
		}
		SoundManager.Instance.SetBusVol((int) SoundManagerBus.Music, value);
        SetSetting(settingName, value);
	}


	private void SetSfxVol(float sfxVol)
	{
		var settingName = AudioSettingType.SfxVol.ToString();
		var value = sfxVol;
		try
		{
			if (value is < MinVol or > MaxVol)
				throw new ArgumentOutOfRangeException();
		}
		catch
		{
			value = GetDefaultSetting<float>(settingName);
		}
		SoundManager.Instance.SetBusVol((int) SoundManagerBus.Sfx, value);
        SetSetting(settingName, value);
		
	}
	
	

}