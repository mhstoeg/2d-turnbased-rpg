using System;
using Godot;

namespace TurnBasedRPG.entities.settings.gameplay;



public partial class GameplaySettings : BaseSettingCategory
{
	public static GameplaySettings Instance { get; private set; }
	public const double MinMouseSens = 0.05;
	public const double MaxMouseSens = 3;
	public const double DefaultMouseSens = 1;
		
	[Signal] public delegate void MouseSensChangedEventHandler(double value);
	public override void _Ready()
	{
        base._Ready();
        Instance = this;
		CategoryName = "GAMEPLAY";
		
		DefaultSettings.Add(GameplaySettingType.MouseSens.ToString(), DefaultMouseSens);
	}

	
	public override void ApplySettings()
	{
		// TODO Apply changes to only the settings that were actually changed, disable change buttons if they were
		// TODO manually reverted
		var settingName = GameplaySettingType.MouseSens.ToString();
		SetMouseSens(GetSetting<double>(settingName));
	}

	public void SetMouseSens(double mouseSens)
	{
		var settingName = GameplaySettingType.MouseSens.ToString();
		var value = mouseSens;
		try
		{
			if (value is < MinMouseSens or > MaxMouseSens)
				throw new ArgumentOutOfRangeException();
		}
		catch
		{
			value = GetDefaultSetting<double>(settingName);
		}

		EmitSignal(SignalName.MouseSensChanged, value);
        SetSetting(settingName, value);
	}
}