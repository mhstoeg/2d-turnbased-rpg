using System;
using Godot;
using Godot.Collections;
using TurnBasedRPG.debug;

namespace TurnBasedRPG.entities.settings;

public abstract partial class BaseSettingCategory : Node
{
    
    public string CategoryName { get; protected set; }
    public Dictionary Settings { get; protected set; }
    public Dictionary DefaultSettings { get; protected set; }
    private const int NumericalSettingDecimalPlace = 2;

    public override void _Ready()
    {
        base._Ready();
		Settings = new Dictionary();
		DefaultSettings = new Dictionary();
    }

    public void SetUserSettings(Dictionary userSettings)
    {
        foreach (string key in userSettings.Keys)
        {
            if (key != CategoryName) continue;
            Settings = (Dictionary) userSettings[key];
            return;
        }
    }

    public void SetToDefault()
    {
        Settings = DefaultSettings.Duplicate(true);
    }

    public void SetSetting<[MustBeVariant] T>(string key, T value)
    {
        if (typeof(T) == typeof(double))
        {
            var roundedValue = Math.Round(Convert.ToDecimal(value), NumericalSettingDecimalPlace);
            value = (T) Convert.ChangeType(roundedValue, typeof(T));
        }
            
        Settings[key] = Settings.ContainsKey(key) ? Variant.From(value) : Variant.From(GetDefaultSetting<T>(key));
    }

    public T GetSetting<[MustBeVariant] T>(string key)
    {
        var result = Settings.TryGetValue(key, out var setting) ? setting : Variant.From(GetDefaultSetting<T>(key));
        return result.As<T>();
    }

    public T GetDefaultSetting<[MustBeVariant] T>(string key)
    {
        var result = DefaultSettings.TryGetValue(key, out var setting) ? setting : default;
        Debug.Assert(result.VariantType != Variant.Type.Nil, 
            "ERROR: could not get default setting, no default setting exists for " + CategoryName + ": " + key);
        Debug.Assert(result.As<T>().GetType() == typeof(T), 
            "ERROR: default setting for " + key + " is not of the correct type. Got: " 
            + result.VariantType + " Requires: " + (result.As<T>().GetType() == typeof(T)));
            
        return result.As<T>();
    }

    public abstract void ApplySettings();
    
}