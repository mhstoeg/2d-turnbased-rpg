using Godot;
using Godot.Collections;
using AudioSettings = TurnBasedRPG.entities.settings.audio.AudioSettings;
using GameplaySettings = TurnBasedRPG.entities.settings.gameplay.GameplaySettings;
using VideoSettings = TurnBasedRPG.entities.settings.video.VideoSettings;

namespace TurnBasedRPG.entities.settings;

public partial class SettingsManager : Node
{
	public static SettingsManager Instance { get; private set; }
	private BaseSettingCategory[] SettingCategories { set; get; }
	public override void _Ready()
	{
		Instance = this;
		SettingCategories = new BaseSettingCategory[]
		{
			AudioSettings.Instance,
			VideoSettings.Instance,
			GameplaySettings.Instance
		};
	}

	public Dictionary GetSettingsCopy()
	{
		var settings = new Dictionary();
		foreach (var category in SettingCategories)
			settings[category.CategoryName] = category.Settings;
		return settings.Duplicate(true);
	}

	public void SetToDefault()
	{
		foreach (var category in SettingCategories)
			category.SetToDefault();
	}

	public void SetToUserSettings(Dictionary userSettings)
	{
		foreach (var category in SettingCategories)
			category.SetUserSettings(userSettings);
	}

	public void ApplySettings()
	{
		foreach (var category in SettingCategories)
			category.ApplySettings();
		util.SettingsFileAccess.SaveSettings();
	}

}