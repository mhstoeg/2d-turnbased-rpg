using System;
using Godot;

namespace TurnBasedRPG.entities.state;

public abstract partial class BaseStateMachine : Node
{
	private enum NullState 
	{
		Null
	}
	
	private Enum _state = NullState.Null;
	private Enum _previousState = NullState.Null;
	
	[Signal] public delegate void StateChangedEventHandler(int newState, int oldState);


	public override void _PhysicsProcess(double delta)
	{
		base._PhysicsProcess(delta);

		if (State.Equals(NullState.Null)) return;
		
		StateLogic(delta);
		var transition = GetTransition(delta);
		if (!transition.Equals(NullState.Null))
			SetState(transition);
	}

	protected abstract void StateLogic(double delta);
	
	protected virtual Enum GetTransition(double delta)
	{
		return NullState.Null;
	}

	protected abstract void EnterState(Enum newState, Enum oldState);
	protected abstract void ExitState(Enum oldState, Enum newState);
	
	private void SetState(Enum newState)
	{
		_previousState = _state;
		_state = newState;

		if (!_previousState.Equals(NullState.Null))
			ExitState(_previousState, newState);

		if (!newState.Equals(NullState.Null))
			EnterState(newState, _previousState);

		EmitSignal(SignalName.StateChanged);
	}

	public bool IsState(Enum state)
	{
		return state.Equals(_state);
	}
	
	public Enum State
	{
		get => _state;
		set => SetState(value);
	}

	
}