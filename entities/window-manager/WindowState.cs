﻿namespace TurnBasedRPG.entities.window_manager;

public enum WindowState
{
    Windowed,
    Fullscreen,
    ExclusiveFullscreen
}