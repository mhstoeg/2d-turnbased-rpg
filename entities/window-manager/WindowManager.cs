using System;
using Godot;
using TurnBasedRPG.entities.settings.video;
using Vector2 = System.Numerics.Vector2;
using VideoSettings = TurnBasedRPG.entities.settings.video.VideoSettings;

namespace TurnBasedRPG.entities.window_manager;


public partial class WindowManager : Node
{
	public static WindowManager Instance { get; private set; }
    private WindowStateMachine StateMachine { get; set; }
    public const int MinVsyncMode = 0;
    public const int MaxVsyncMode = 1;
    public const int MinFpsMax = 30;
    public const int MaxFpsMax = 1000;

    public override void _Ready()
    
    {
        base._Ready();
        Instance = this;
		StateMachine = GetNode<WindowStateMachine>("StateMachine");
        // Connect signal to readjust window scale when the window's size is changed, e.g maximising, resizing etc...
        GetTree().Root.Connect(Viewport.SignalName.SizeChanged, new Callable(this, WindowManager.MethodName.AlignScale));
    }

    public void AlignScale()
    {
        var viewport = GetTree().Root.Size;
        GetTree().Root.ContentScaleSize = viewport;
    }

    public void SetWindowState(int windowState)
    {
        var settingName = VideoSettingType.DisplayOption.ToString();
        var value = windowState;
        try
        {
            if (value < 0 || value >= Enum.GetNames(typeof(WindowState)).Length) 
                throw new ArgumentOutOfRangeException();
        }
        catch
        {
            value = VideoSettings.Instance.GetDefaultSetting<int>(settingName);
        }
        StateMachine.State = (WindowState) value;
        VideoSettings.Instance.SetSetting(settingName, value);
    }

    public void SetResolution(Vector2 resolution)
    {
    }

    public void SetMonitor(int index)
    {
        var settingName = VideoSettingType.MonitorOption.ToString();
        var value = index;
        try
        {
            if (value <0 || value >= DisplayServer.GetScreenCount()) 
                throw new ArgumentOutOfRangeException();
        }
        catch
        {
            value = VideoSettings.Instance.GetDefaultSetting<int>(settingName);
        }
        DisplayServer.WindowSetCurrentScreen(value);
        VideoSettings.Instance.SetSetting(settingName, value);
    }

    public void SetVsyncMode(int vsyncMode)
    {
        var settingName = VideoSettingType.Vsync.ToString();
        var value = vsyncMode;
        try
        {
            if (value is < MinVsyncMode or > MaxVsyncMode)
                throw new ArgumentOutOfRangeException();
        }
        catch
        {
            value =  VideoSettings.Instance.GetDefaultSetting<int>(settingName);
        }
        DisplayServer.WindowSetVsyncMode((DisplayServer.VSyncMode) value);
        VideoSettings.Instance.SetSetting(settingName, value);
    }

    public void SetMaxFps(int maxFps)
    {
        var settingName = VideoSettingType.MaxFps.ToString();
        var value = maxFps;
        try
        {
            if (value != 0 && (value is < MinFpsMax or > MaxFpsMax))
                throw new ArgumentOutOfRangeException();
        }
        catch
        {
            value =  VideoSettings.Instance.GetDefaultSetting<int>(settingName);
            GD.Print(value);
        }
        Engine.MaxFps = value;
        VideoSettings.Instance.SetSetting(settingName, value);

    }

}