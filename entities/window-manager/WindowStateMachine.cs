using System;
using Godot;
using BaseStateMachine = TurnBasedRPG.entities.state.BaseStateMachine;

namespace TurnBasedRPG.entities.window_manager;

public partial class WindowStateMachine : BaseStateMachine
{

	protected override void StateLogic(double delta)
	{
		switch (State)
		{
			case WindowState.Windowed:
				break;
			case WindowState.Fullscreen:
				break;
			case WindowState.ExclusiveFullscreen:
				break;
		}
	}

	protected override Enum GetTransition(double delta)
	
	{
		var transition = base.GetTransition(delta);
		switch (State)
		{
			case WindowState.Windowed:
				break;
			case WindowState.Fullscreen:
				break;
			case WindowState.ExclusiveFullscreen:
				break;
		}

		return transition;
	}

	protected override void EnterState(Enum newState, Enum oldState)
	{
		switch (newState)
		{
			case WindowState.Windowed:
				DisplayServer.WindowSetMode(DisplayServer.WindowMode.Windowed);
				DisplayServer.WindowSetSize(DisplayServer.ScreenGetSize(DisplayServer.WindowGetCurrentScreen()) / 2);
				DisplayServer.WindowSetFlag(DisplayServer.WindowFlags.Borderless, false,
					DisplayServer.WindowGetCurrentScreen());
				DisplayServer.WindowSetFlag(DisplayServer.WindowFlags.ResizeDisabled, false,
					DisplayServer.WindowGetCurrentScreen());
				break;
			case WindowState.Fullscreen:
				DisplayServer.WindowSetMode(DisplayServer.WindowMode.Fullscreen);
				break;
			case WindowState.ExclusiveFullscreen:
				DisplayServer.WindowSetMode(DisplayServer.WindowMode.ExclusiveFullscreen);
				DisplayServer.MouseSetMode(DisplayServer.MouseMode.Confined);
				break;
			default:
				throw new Exception("Entered invalid State, " + State);
		}
		WindowManager.Instance.AlignScale();
	}

	protected override void ExitState(Enum oldState, Enum newState)
	{
		switch (oldState)
		{
			case WindowState.Windowed:
				DisplayServer.WindowSetFlag(DisplayServer.WindowFlags.Borderless, true,
					DisplayServer.WindowGetCurrentScreen());
				DisplayServer.WindowSetFlag(DisplayServer.WindowFlags.ResizeDisabled, true,
					DisplayServer.WindowGetCurrentScreen());
				break;
			case WindowState.Fullscreen:
				break;
			case WindowState.ExclusiveFullscreen:
				DisplayServer.MouseSetMode(DisplayServer.MouseMode.Visible);
				break;
			default:
				throw new Exception("Exited invalid State, " + State);
		}
	}

}