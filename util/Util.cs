using Godot;

namespace TurnBasedRPG.util;

public partial class Util : Node
{
	public static Util Instance { get; private set; }
    public override void _Ready()
    {
        base._Ready();
        Instance = this;
    }

    public Image Screenshot()
    {
        return GetViewport().GetTexture().GetImage();
    }
    
	private static void StopNodeProcess(Node node, bool stop)
	{
		// TODO remove this??
		node.SetProcess(!stop);
		node.SetProcessInput(!stop);
		node.SetProcessInternal(!stop);
		node.SetProcessShortcutInput(!stop);
		node.SetProcessUnhandledInput(!stop);
		node.SetProcessUnhandledKeyInput(!stop);
		node.SetPhysicsProcess(!stop);
		node.SetPhysicsProcessInternal(!stop);
		
	}
}