﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Godot;
using Godot.Collections;
using Range = System.Range;

namespace TurnBasedRPG.util;

public static class Calculate
{
    public static int DecimalPlace(float number)
    {
		var numberString = number.ToString(CultureInfo.InvariantCulture);
		return numberString[(numberString.IndexOf(".", StringComparison.Ordinal) + 1)..].Length;
    }
    
	public static Vector2I[] CoordsWithinRadius(Vector2I coords, Vector2I xRange, Vector2I yRange, int radius, bool returnCenter = false)
	{
		var result = new List<Vector2I>();
		for (var i = -1 * radius; i <= radius; i++)
		{
			var newXCoord = coords.X + i;
			if (!(newXCoord >= xRange.X) || !(newXCoord <= xRange.Y)) continue;
			
			for (var j = -1 * radius; j <= radius; j++)
			{
				var newYCoord = coords.Y + j;
				if (!(newYCoord >= yRange.X) || !(newYCoord <= yRange.Y)) continue;
				
				result.Add(new Vector2I(newXCoord, newYCoord));
			}
		}

		if (!returnCenter)
		{
			result.Remove(coords);
		}

		return result.ToArray();
	}

	// public static Array<Vector2> CoordsOnEdgeOfRadius(Vector2 coords, int radius)
	// {
	// 	var result = new Array<Vector2>();
	// 	// TODO account for within x coord range
	// 	for (var i = -1 * radius; i <= radius; i++)
	// 	{
	// 		// TODO account for within y coord range
	// 		for (var j = -1 * radius; j <= radius; j++)
	// 		{
	// 			// Only add locations of edge of the radius
	// 			if ((i == -1 * radius || i == radius || j == -1 * radius || j == radius))
	// 			{
	// 				result.Add(new Vector2(coords.X + i, coords.Y + j));
	// 			}
	// 		}
	// 	}
	//
	// 	return result;
	// }
}