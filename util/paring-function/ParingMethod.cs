﻿namespace TurnBasedRPG.util.paring_function;

public enum ParingMethod
{
    CantorUnsigned, // positive values only
    CantorSigned, // both positive and negative values
    SzudzikUnsigned, // more efficient than cantor
    SzudzikSigned, // both positive and negative values
    SzudzikImproved, // improved version (best option)
}