﻿using Godot;
using TurnBasedRPG.debug;

namespace TurnBasedRPG.util.paring_function;

public static class ParingFunction
{
    public static int PairVector2(Vector2 pointPosition, ParingMethod pairingMethod)
    {
        var a = (int) pointPosition.X;
        var b = (int) pointPosition.Y;
        switch (pairingMethod)
        {
            case ParingMethod.CantorUnsigned:
                Debug.Assert(a >= 0 && b >= 0, "Board: pairing method has failed. Choose method that supports negative values");
                return CantorPair(a, b);
            case ParingMethod.SzudzikUnsigned:
                Debug.Assert(a >= 0 && b >= 0, "Board: pairing method has failed. Choose method that supports negative values");
                return SzudzikPair(a, b);
            case ParingMethod.CantorSigned:
                return CantorPairSigned(a, b);
            case ParingMethod.SzudzikSigned:
                return SzudzikPairSigned(a, b);
            case ParingMethod.SzudzikImproved:
                return SzudzikPairImproved(a, b);
            default:
                return SzudzikPairImproved(a, b);
        }

    }

    private static int CantorPair(int a, int b)
    {
        var result = 0.5 * (a + b) * (a + b + 1) + b;
        return (int)result;
    }
    private static int CantorPairSigned(int a, int b)
    {
        if (a >= 0) a *= 2;
        else a = (a * -2) - 1;

        if (b >= 0) b *= 2;
        else b = (b * -2) - 1;

        return CantorPair(a, b);
    }

    private static int SzudzikPair(int a, int b)
    {
        if (a >= b) return (a * a) + a + b;
        return (b * b) + a;
    }
    private static int SzudzikPairSigned(int a, int b)
    {
        if (a >= 0) a *= 2;
        else a = (a * -2) - 1;

        if (b >= 0) b *= 2;
        else b = (b * -2) - 1;
        
        return SzudzikPair(a, b);


    }
    private static int SzudzikPairImproved(int x, int y)
    {
        int a;
        int b;

        if (x >= 0) a = x * 2;
        else a = (x * -2) - 1;

        if (y >= 0) b = y * 2;
        else b = (y * -2) - 1;

        var c = SzudzikPair(a, b);

        if ((a >= 0 && b < 0) || (b >= 0 && a < 0))
            return -1*c - 1;
        return c;

    }
}