using System.Threading.Tasks;
using Godot;

namespace TurnBasedRPG.util;

public partial class GlobalAnimation : Node
{
	public static GlobalAnimation Instance { get; private set; }
    public override void _Ready()
    {
        base._Ready();
        Instance = this;
    }

    public async Task Slide(Control node, Tween tween, double duration, 
        bool slideOn, bool slideX, bool slideY, bool flipX, bool flipY)
    {
        //TODO Maybe not use Control Node??
        var screenSize = DisplayServer.WindowGetSize();
        
        var finalPosition = new Vector2(slideX ? screenSize.X : 0, slideY ? screenSize.Y : 0);
        finalPosition.X = flipX ? -finalPosition.X: finalPosition.X;
        finalPosition.Y = flipY ? -finalPosition.Y: finalPosition.Y;

        node.Position = slideOn ? finalPosition : Vector2.Zero;
        
        tween.TweenProperty(node, "position", slideOn ? Vector2.Zero : finalPosition, duration);

        await ToSignal(tween, Tween.SignalName.Finished);
    }

}