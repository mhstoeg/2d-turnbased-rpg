using Godot;
using Godot.Collections;
using SettingsManager = TurnBasedRPG.entities.settings.SettingsManager;

namespace TurnBasedRPG.util;

public partial class SettingsFileAccess : Node
{
	private const string DirPath = "user://";
	private const string Filename = "settings.json";
	

	public static void SaveSettings()
	{
		var file = FileAccess.Open(DirPath + Filename, FileAccess.ModeFlags.Write);
		file.StoreString(Json.Stringify(SettingsManager.Instance.GetSettingsCopy(), "\t"));
		file.Close();
	}

	public static void LoadSettings()
	{
		if (!DirAccess.DirExistsAbsolute(DirPath))
			DirAccess.MakeDirAbsolute(DirPath);
		
		var file = FileAccess.Open(DirPath + Filename, FileAccess.ModeFlags.Read);
		var userSettings = file != null ? (Dictionary)Json.ParseString(file.GetAsText()) : null;
		
		if (userSettings != null) 
			SettingsManager.Instance.SetToUserSettings(userSettings);
		else SettingsManager.Instance.SetToDefault();

		file?.Close();
	}
}